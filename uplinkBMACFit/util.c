#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <sched.h>
#include <assert.h>
#include <fcntl.h>
#include <sys/syscall.h>
#include <ecotools/errordefs.h>
#include <ecotools/roi_hooks.h>
#include <ecotools/cpu_uarch.h>
#include "kernel_def.h"
#include "common_utils.h"
#include "util.h"

inline void spin_on_flag(volatile lte_atomic_t *flg, uint32_t delta) {
	/**
	 * Spins on the atomic flag
	 * flg after sleeping for
	 * every delta time
	 * Clear the flag after
	 * spinning on it.
	 */
	while(!atomic_load(flg));
	atomic_store(flg,false); /* reset it once again */
}

inline void test_and_set_flag(volatile lte_atomic_t *flg) {
	assert(atomic_exchange(flg,true) == false);
}

/* Compare the two users in terms of their workload */
int cmp_ue(const void *a, const void *b) {
	int i;
	const ue_t *ueap = (ue_t*)a;
	const ue_t *uebp = (ue_t*)b;

	if (ueap->nmb_rb < uebp->nmb_rb)
		return -1;
	else if (ueap->nmb_rb == uebp->nmb_rb)
		return 0;
	else
		return 1;
}

unsigned int print_ue(FILE *fd, const ue_t *uep) {
	char modstr[BUFSIZ];
	switch(uep->mod) {
		case MOD_PSK 	: sprintf(modstr,"%s","MOD_PSK");   break;
		case MOD_QPSK 	: sprintf(modstr,"%s","MOD_QPSK");  break;
		case MOD_16QAM  : sprintf(modstr,"%s","MOD_16QAM"); break;
		case MOD_64QAM  : sprintf(modstr,"%s","MOD_64QAM"); break;
		default			: PRINTERROR("%d:Unable to print, modulation scheme not supported\n",uep->mod);
	}
	fprintf(fd,"frame:%d,sf:%d,ueid:%d,nmb_layer:%d,start_rb:%d,nmb_rb:%d,modstr:%s,last:%s\n",\
	uep->frame,\
	uep->subframe,uep->ueid,\
	uep->nmb_layer,uep->start_rb,\
    uep->nmb_rb,modstr,\
	(uep->last)?"True":"False");
	return (uep->nmb_rb);
}

void print_task3(FILE *fd, const config_task3_t* task3) {
	char modstr[BUFSIZ];
	switch(task3->task_type) {
		case MICF 	    : sprintf(modstr,"%s","MICF");   break;
		case COMBWC 	: sprintf(modstr,"%s","COMBWC");  break;
		case ANTCOMB    : sprintf(modstr,"%s","ANTCOMB"); break;
		case DEMAP      : sprintf(modstr,"%s","DEMAP"); break;
		default			: PRINTERROR("%d:task type not supported\n",task3->task_type);
	}
	
	fprintf(fd,"task3:{sf:%d,ueid:%d,tasktype:%s,taskid:%d,slot:%d}\n",\
                task3->subframe,task3->userid,\
                modstr,\
                task3->taskid,task3->slotid);
}

/* Void Print the subframes, Perform some error checking as well */
void print_sf(FILE *fd, const ue_t *sf, unsigned int nmbUsers) {
	unsigned int i;
	unsigned int total_prbs = 0;
	fprintf(fd,"-----------------------\n");
	for(i = 0; i < nmbUsers; i++)
		total_prbs += print_ue(stdout,&sf[i]);
	if (total_prbs > 100) {
		fprintf(stderr,"Malformed Subframe@%d, total_prbs exceeds 100\n",sf[0].subframe);
		exit(EXIT_FAILURE);
	}
	if (nmbUsers > 10) {
		fprintf(stderr,"Malformed Subframe@%d, total_ue exceeds 10\n",sf[0].subframe);
		exit(EXIT_FAILURE);
	}
	if (nmbUsers > 0) {
		if (!sf[nmbUsers-1].last) {
			fprintf(stderr,"Malformed Subframe@%d, Last UE not appropriately tagged\n",sf[0].subframe);
			//exit(EXIT_FAILURE);
		}
	}
	fprintf(fd,"Total PRBs allocated = %d\n",total_prbs);
	fprintf(fd,"Total UEs allocated = %d\n",nmbUsers);
	fprintf(fd,"-----------------------\n");
}