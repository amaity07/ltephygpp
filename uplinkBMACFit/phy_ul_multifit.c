#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <sched.h>
#include <assert.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <ecotools/errordefs.h>
#include <ecotools/roi_hooks.h>
#include <ecotools/cpu_uarch.h>
#include "kernel_def.h"
#include "common_utils.h"
#include "util.h"
#include "ant_comb_7.h"
#include "chest_5.h"
#include "complex_def.h"
#include "crc_13.h"
#include "fft_8.h"
#include "interleave_11.h"
#include "mf_4.h"
#include "soft_demap_9.h"
#include "turbo_dec_12.h"
#include "weight_calc_6.h"
#include "multifit.h"
#include "sfgen.h"

//#define SLEEP_BW_ACTIVATION

static inline void compute_micf(mfit_task4_t *task4, int slot) {
	int subframe = task4->subframe;
	int userid	 = task4->userid;
	int taskid	 = task4->taskid;
	int nmbLayer = task4->nmbLayer;
	int startSc	 = task4->startSc;
	int nmbSc	 = task4->nmbSc;

	if(taskid < nmbLayer * RX_ANT) {
		input_data_t 	*ue_data_p 	= &(pregen_data_array[subframe%NMB_DATA_FRAMES]);
		phy_vars_t 		*phy_vars_p = &(phy_vars_mue[userid]);
		int layer 	 = taskid / RX_ANT;
		int rx		 = taskid % RX_ANT;

		/* Matched Filter */
		mf(&(ue_data_p->in_data[slot][3][rx][startSc]), 	 \
		   &(ue_data_p->in_rs[slot][startSc][layer]), /* Check the indices*/ \
		   nmbSc, phy_vars_p->layer_data[layer][rx],  \
		   &(phy_vars_p->pow[layer][rx]));
		/* IFFT */
		ifft(phy_vars_p->layer_data[layer][rx],			\
			 nmbSc,										\
			 ue_data_p->fftw[slot]);
		/* Windowing */
		window(phy_vars_p->layer_data[layer][rx],			\
			  phy_vars_p->pow[layer][rx],					\
			  nmbSc,										\
			  phy_vars_p->layer_data[layer][rx],			\
			  &(phy_vars_p->res_power[layer][rx]));
		/* Intermediate Data Storage */
		phy_vars_p->R[layer][rx].re = 0.0;
		phy_vars_p->R[layer][rx].im = 0.0;
		/* FFT */
		fft(phy_vars_p->layer_data[layer][rx],\
		nmbSc,ue_data_p->fftw[slot]);
	}
}

static inline void compute_combwc(mfit_task4_t *task4, int slot) {
	int subframe = task4->subframe;
	int userid	 = task4->userid;
	int taskid	 = task4->taskid;
	int nmbLayer = task4->nmbLayer;
	int nmbSc	 = task4->nmbSc;

	if(taskid < MAX_COMBWC_TASKS) {
		input_data_t 	*ue_data_p 	= &(pregen_data_array[subframe%NMB_DATA_FRAMES]);
		phy_vars_t 		*phy_vars_p = &(phy_vars_mue[userid]);
		
		/* Calculate the range of subcarriers to compute on */
		assert(nmbSc % MAX_COMBWC_TASKS == 0);
		int start_tile				= (taskid)*(nmbSc/MAX_COMBWC_TASKS);
		int end_tile				= (taskid+1)*(nmbSc/MAX_COMBWC_TASKS);

		comb_w_calc_partiled_generic(phy_vars_p->layer_data,		\
							 start_tile,end_tile,nmbLayer,	\
							 phy_vars_p->R,					\
							 phy_vars_p->comb_w);
	}
}

static inline void compute_antcomb(mfit_task4_t *task4, int slot) {
	int subframe = task4->subframe;
	int userid	 = task4->userid;
	int taskid	 = task4->taskid;
	int nmbLayer = task4->nmbLayer;
	int nmbSc	 = task4->nmbSc;
	int startSc	 = task4->startSc;

	if(taskid < nmbLayer*(OFDM_IN_SLOT-1)) {
		ltephygpp_complex_t* in[RX_ANT];
		int index_out;
		int ofdm_count;
		int ofdm;
		int layer, rx;
		int sc, base_idx;

		input_data_t 	*ue_data_p 	= &(pregen_data_array[subframe%NMB_DATA_FRAMES]);
		phy_vars_t 		*phy_vars_p = &(phy_vars_mue[userid]);

		layer       = taskid / (OFDM_IN_SLOT-1);
		ofdm_count  = taskid % (OFDM_IN_SLOT-1);
		ofdm        = (ofdm_count > 2)?(ofdm_count+1):ofdm_count;

		for(rx = 0; rx < RX_ANT; rx++) {
			in[rx] = &(ue_data_p->in_data[slot][ofdm][rx][startSc]);
		}
		/* Put all demodulated symbols in one long vector */
		index_out = nmbSc*ofdm_count + slot*(OFDM_IN_SLOT-1)*nmbSc + layer*2*(OFDM_IN_SLOT-1)*nmbSc;
		ant_comb(in, phy_vars_p->combWeight[layer], nmbSc, \
				 &phy_vars_p->symbols[index_out]);
		/* Now transform data back to time plane */
		ifft(&phy_vars_p->symbols[index_out], \
			nmbSc,ue_data_p->fftw[slot]);

	}
}

static inline void compute_demap(mfit_task4_t *task4, int slot) {
	int subframe = task4->subframe;
	int userid	 = task4->userid;
	int taskid	 = task4->taskid;
	int nmbLayer = task4->nmbLayer;
	int nmbSc	 = task4->nmbSc;
	int startSc	 = task4->startSc;
	mod_type mod = task4->mod;
	if(taskid < MAX_DEMAP_TASKS) {
		phy_vars_t	 *phy_vars_p		= &(phy_vars_mue[userid]);
		ltephygpp_complex_t *deint_symbols		= phy_vars_p->deint_symbols;
		char *softbits					= phy_vars_p->softbits;
		float pow						= phy_vars_p->pow[0][0];

		int nmbSymbols  				= NMB_SLOT*nmbSc*(OFDM_IN_SLOT-1)*nmbLayer;
		soft_demap_pthread(deint_symbols, pow, mod, nmbSymbols, softbits, MAX_DEMAP_TASKS, taskid);
	}
}

static void *exec_phy_multifit_task4(void *arg) {
	/* Worker Thread@thr_id */
	mfit_task4_bin_t *bin = (mfit_task4_bin_t*) arg;
	int thr_id                        = bin->my_idx;
	unsigned int total_app_cpus       = bin->total_app_cpus;
	int aff_offset                    = bin->aff_offset;
	unsigned int cpu_affity           = (thr_id+1+aff_offset)%total_app_cpus;
	affinity_set_cpu2(cpu_affity);
	int task4id                       = 0;		/* idx into task4set2 */
	int i                             = 0;
	mfit_task4_t *task4               = NULL;
	int	slot                          = 0; 
	int ue_nmbLayer, ue_nmbSc         = 0;
	int rx, layer, sc;
	int nmbSymbols, nmbSoftbits;

	/* Busy Waiting - to allow all migrations to finish off */
	// PRINTTOPO("Warmup Busy Spins to complete the thread to cpu migrations");
	busy_wait(AFFINE_MIGRATION_SPIN);
	
	
	// PRINTTOPO("Daemon-Thread@%d to-be-affined(%d), actually-affined(%d) (INIT)",\
	thr_id,cpu_affity,sched_getcpu());

	while(1) {
		/* Process the computations allocated to in every subframe */
		for(slot = 0; slot < NMB_SLOT; slot++) {
			/* Wait for phase 1 (MICF) to be configured */
			bin = &task4bins2[0][thr_id]; /* reset the bin assignment, TODO : Clean-it-up */
			spin_on_flag(&(bin->config_ph1[slot]),0);
			/* Execute all the MICF tasks in contained in your bin */
			// PRINTTOPO("ThrId@%d: Activated MICF",thr_id);
			for(i = 0; i < bin->curr_idx; i++) {
				task4id = bin->task4set_indices[i];
				task4   = &task4set2[0][task4id];
				// PRINTTOPO("ThrId@%d,task4id:%d",thr_id,task4id);
				compute_micf(task4,slot);
			}
			test_and_set_flag(&(bin->ph1_done[slot]));


			

			/* Wait for phase 2 (COMBWC) to be configured */
			bin = &task4bins2[1][thr_id]; /* reset the bin assignment, TODO : Clean-it-up */
			spin_on_flag(&(bin->config_ph2[slot]),0);
			/* Execute all the COMBWC tasks in sequence */
			for(i = 0; i < bin->curr_idx; i++) {
				task4id = bin->task4set_indices[i];
				task4  	= &task4set2[1][task4id];
				compute_combwc(task4,slot);
			}
			/* Signal the finish of COMBWC tasks */
			test_and_set_flag(&(bin->ph2_done[slot]));


			/* 
			 * Data Shuffling -
			 * No need to invoke mfit as 
			 * as there are enough threads to
			 * perform data shuffling
			 */
			bin = &task4bins2[0][thr_id]; /* reset the bin assignment, TODO : Clean-it-up */
			spin_on_flag(&(bin->config_phS[slot]),0);
			if(thr_id < bin->nmbUsers) {
				ue_nmbLayer	= mue[thr_id].nmb_layer;
				ue_nmbSc	= (mue[thr_id].nmb_rb) * 12;
			
				for(rx = 0; rx < RX_ANT; rx++) {
					for(layer = 0; layer < ue_nmbLayer; layer++) {
						for(sc = 0; sc < ue_nmbLayer; sc++)
							phy_vars_mue[thr_id].combWeight[layer][sc][rx] =\
							phy_vars_mue[thr_id].comb_w[sc][layer][rx];
					}
				}
			}
			/* Signal the finish of data shuffling*/
			test_and_set_flag(&(bin->phS_done[slot]));



			/* Wait for phase 3 (ANTCOMB) to be configured */
			bin = &task4bins2[2][thr_id]; /* reset the bin assignment, TODO : Clean-it-up */
			spin_on_flag(&(bin->config_ph3[slot]),0);
			/* Execute all the ANTCOMB tasks in sequence */
			for(i = 0; i < bin->curr_idx; i++) {
				task4id = bin->task4set_indices[i];
				task4   = &task4set2[2][task4id];
				compute_antcomb(task4,slot);
			}
			/* Signal the finish of ANTCOMB tasks */
			test_and_set_flag(&(bin->ph3_done[slot]));
		}


		/* Wait for phase 4 (INTRLV) to be configured */
		bin = &task4bins2[0][thr_id]; /* reset the bin assignment, TODO : Clean-it-up */
		spin_on_flag(&(bin->config_intrlv),0);
		if(thr_id < bin->nmbUsers) {
			ue_nmbLayer	= mue[thr_id].nmb_layer;
			ue_nmbSc	= (mue[thr_id].nmb_rb) * 12;
			nmbSymbols  = NMB_SLOT * ue_nmbSc * (OFDM_IN_SLOT - 1) * ue_nmbLayer;
			interleave(&phy_vars_mue[thr_id].symbols[0],&phy_vars_mue[thr_id].deint_symbols[0],nmbSymbols);
			//interleave(&(phy_vars_mue[thr_id].symbols[0]),&(phy_vars_mue[thr_id].deint_symbols[0]),nmbSymbols);
		}
		/* Signal the finish of INTRLV tasks */
		test_and_set_flag(&(bin->intrlv_done));
		

		/* Wait for phase 4 (DEMAP) to be configured */
		bin = &task4bins2[3][thr_id]; /* reset the bin assignment, TODO : Clean-it-up */
		spin_on_flag(&(bin->config_ph4),0);
		/* Execute all the DEMAP tasks in sequence */
		for(i = 0; i < bin->curr_idx; i++) {
			task4id = bin->task4set_indices[i];
			task4   = &task4set2[3][task4id];
			compute_demap(task4,slot);
		}
		/* Signal the finish of ANTCOMB tasks */
		test_and_set_flag(&(bin->ph4_done));


		/* Finish CRC and Turbo-Decoder */
		bin = &task4bins2[0][thr_id]; /* reset the bin assignment, TODO : Clean-it-up */
		spin_on_flag(&(bin->config_crcturbo),0);
		if(thr_id < bin->nmbUsers) {
			ue_nmbLayer	= mue[thr_id].nmb_layer;
			ue_nmbSc	= (mue[thr_id].nmb_rb) * 12;
			nmbSymbols  = NMB_SLOT * ue_nmbSc * (OFDM_IN_SLOT - 1) * ue_nmbLayer;
			nmbSoftbits = nmbSymbols * mue[thr_id].mod;
			turbo_dec(nmbSoftbits);
			crcFast(phy_vars_mue[thr_id].bits,nmbSoftbits/24);
		}
		/* Signal the finish of Turbo-Dec-CRC */
		test_and_set_flag(&(bin->crcturbo_done));
	}
}

void deinit() {
	int i;
	PRINTTOPO("De-initializing and De-allocation FFT Handles");
	// for(i = 0; i < 100; i++)
	// 	fft_deinit(&global_fft_handle[i]);
}

static void init_globals(unsigned int numSlaveBins,\
				  		const char *traceFileName,\
				  		const char *dumpFileName) {
	cpu_topology_t cpu_top;
	int total_app_cpus = numSlaveBins+1;
	int aff_offset = 0;

	/* Initialze the pregenerated (input) data */
	#ifndef QUIET
	PRINTTOPO("Initializing Input Dataset");
	#endif
	init_input_data(),
	crcInit();
	
	/* Initialize the bins */
	#ifndef QUIET
	PRINTTOPO("Initializing Bins");
	#endif
	int phase = 0;
	for(phase = 0; phase < 4; phase++)
		task4bin_init(&task4bins2[phase][0],numSlaveBins,&task4bin_weights2[phase][0],true,0);

	/* Initialize the trace files */
	#ifndef QUIET
	PRINTTOPO("Opening PHY Trace file and performance traces");
	#endif
	init_trc_file(traceFileName);
	#ifdef INSTRUMENTUE
	ueetGroundTruthFD2 = fopen(dumpFileName,"w");
	setvbuf(ueetGroundTruthFD2,ueetGroundTruthFD2buf,ueetGroundTruthFD2buf ? _IOFBF : _IONBF,PDCBUFSIZE);
	fprintf(ueetGroundTruthFD2,"ueid,subframe,slice,replicationId,SchedOverhead,start,startProcessing,end\n");
	#endif

	/* Affine the main thread */
	#ifndef QUIET
	PRINTTOPO("Affining the main thread at cpu@%d",sched_getcpu());
	#endif
	affinity_set_cpu2(aff_offset);


	/* Generate the worker threads */
	#ifndef QUIET
	PRINTTOPO("Creating %d Daemon Threads",numSlaveBins);
	#endif
	int i;
	for(i = 0; i < numSlaveBins; i++) {
		for(phase = 0; phase < 4; phase++) {
			task4bins2[phase][i].my_idx            = i;
			task4bins2[phase][i].ht_enable         = false;
			task4bins2[phase][i].total_app_cpus    = total_app_cpus;
			task4bins2[phase][i].aff_offset        = aff_offset;
		}
		/* The bins are re-assigned accordingly */
		if(pthread_create(&th[i],NULL,exec_phy_multifit_task4,&task4bins2[0][i]) < 0) {
			PRINTERROR("Thread@%d Creation failed \n",i);
		}
	}
}

int phy_G_multifit(int numSlaveBins,\
               const char *traceFileName,\
			   const char *dumpFileName,\
			   int version)  {
	
	
	#ifndef QUIET
	PRINTTOPO("Number of BS = %d",MAX_BS);
	PRINTTOPO("Base Station Parameters = %d X %d MIMO",\
	MIMO_CONFIGURATION,MIMO_CONFIGURATION);
	#endif

	/* Subframe parameters */
	int nmbUsers     = 0;
	struct timespec orig, sf_start, sf_mid, sf_stop, bench_stop;
	#ifdef SLEEP_BW_ACTIVATION
	int sleep_time             = 0;
	int delta		           = 2000; /* Interarrival time b/w subframes */
	#endif
	float start_socket_energy  = 0.0;
	float start_dram_energy    = 0.0;
	int subframe_count         = 0;
	int window		           = 0;
	int ue			           = 0;
	int task4set_idx           = 0;
	int slot		           = 0;
	float C	   		           = 0.0;
	int bin_idx		           = 0;
	int i			           = 0;
	bool ht_enable             = false;
	int userIndex              = 0;
	double sfet                = 0.0;
	double sfrtPrev            = 0.0; /* Execution time of the previous subframe */
	double rel_subframe_start      = 0.0;
	double sfrt = 0.0;
	double subframe_arrival = 0.0;
	double schedComp;
	init_globals(numSlaveBins,traceFileName,dumpFileName);
	
	/* Measure the Synchronization of a particular phase say COMBWC */
	busy_wait(1000);                 /* Optional busy wait */

	/* Return Here Generate Tasks */
	PRINTTOPO("-------------START Generating Subframes (%d)---------------",numSlaveBins);
	clock_gettime(CLOCK_REALTIME,&orig);
	while(1) {
		nmbUsers = gen_sf3(mue,MAX_USERS,version);
		if (nmbUsers < 0)
			break;
		
		#ifdef PRINT_HOOKS
		print_sf(stdout,mue,nmbUsers);
		#endif

		/* GENERATE SCHEDULES (use multifit packing) */
		clock_gettime(CLOCK_REALTIME,&sf_start);
		if (nmbUsers > 0) {
			qsort(mue,nmbUsers,sizeof(ue_t),cmp_ue);

			/* MICF */
			task4set_idx = 0;
			for(ue = 0; ue < nmbUsers; ue++) {
			create_task4set(&mue[ue],MICF,&task4set2[0][0],task4set_idx,-1);
			task4set_idx += RX_ANT*mue[ue].nmb_layer;
			}
			compute_task4_weight(&task4set2[0][0],task4set_idx,&task4set_weight2[0][0]);
			task4bin_init(&task4bins2[0][0],numSlaveBins,&task4bin_weights2[0][0],false,nmbUsers);
			C = MULTIFIT(&task4set_weight2[0][0],task4set_idx,\
			numSlaveBins,MFIT_ITER,&task4bin_weights2[0][0],subframe_count,-1,0,slot);

			/* COMBWC */
			task4set_idx = 0;
			for(ue = 0; ue < nmbUsers; ue++) {
			usleep(1);	/* Spurious Sleep added, find a way to do away with it*/
			create_task4set(&mue[ue],COMBWC,&task4set2[1][0],task4set_idx,slot);
			task4set_idx += MAX_COMBWC_TASKS;
			}
			compute_task4_weight(&task4set2[1][0],task4set_idx,&task4set_weight2[1][0]);
			task4bin_init(&task4bins2[1][0],numSlaveBins,&task4bin_weights2[1][0],false,nmbUsers);
			C = MULTIFIT(&task4set_weight2[1][0],task4set_idx,\
			numSlaveBins,MFIT_ITER,&task4bin_weights2[1][0],subframe_count,-1,1,slot);

  			/* ANTCOMB */
			task4set_idx = 0;
			for(ue = 0; ue < nmbUsers; ue++) {
				create_task4set(&mue[ue],ANTCOMB,&task4set2[2][0],task4set_idx,slot);
				task4set_idx += (OFDM_IN_SLOT-1)*mue[ue].nmb_layer;
			}
			compute_task4_weight(&task4set2[2][0],task4set_idx,&task4set_weight2[2][0]);
			task4bin_init(&task4bins2[2][0],numSlaveBins,&task4bin_weights2[2][0],false,nmbUsers);
			C = MULTIFIT(&task4set_weight2[2][0],task4set_idx,\
			numSlaveBins,MFIT_ITER,&task4bin_weights2[2][0],subframe_count,-1,2,slot);

  			/* DEMAP */
			task4set_idx = 0;
			for(ue = 0; ue < nmbUsers; ue++) {
				create_task4set(&mue[ue],DEMAP,&task4set2[3][0],task4set_idx,slot);
				task4set_idx += MAX_DEMAP_TASKS;
			}
			compute_task4_weight(&task4set2[3][0],task4set_idx,&task4set_weight2[3][0]);
			task4bin_init(&task4bins2[3][0],numSlaveBins,&task4bin_weights2[3][0],false,nmbUsers);
			C = MULTIFIT(&task4set_weight2[3][0],task4set_idx,\
			numSlaveBins,MFIT_ITER,&task4bin_weights2[3][0],subframe_count,-1,3,-1);
		}	
		clock_gettime(CLOCK_REALTIME,&sf_mid);
		
		/* START COMPUTATION */
		if (nmbUsers > 0) {
			for(slot = 0; slot < NMB_SLOT; slot++) {
				// PRINTTOPO("Enabling MICF flags");
				for(bin_idx = 0; bin_idx < numSlaveBins; bin_idx++) {
					test_and_set_flag(&(task4bins2[0][bin_idx].config_ph1[slot]));
				}
				/* Wait for MICF tasks to end */
				for(bin_idx = 0; bin_idx < numSlaveBins; bin_idx++) {
					spin_on_flag(&(task4bins2[0][bin_idx].ph1_done[slot]),0);
				}


				for(bin_idx = 0; bin_idx < numSlaveBins; bin_idx++) {
					test_and_set_flag(&(task4bins2[1][bin_idx].config_ph2[slot]));
				}
				/* Wait for COMBWC tasks to end */
				for(bin_idx = 0; bin_idx < numSlaveBins; bin_idx++) {
					spin_on_flag(&(task4bins2[1][bin_idx].ph2_done[slot]),0);
				}


				for(bin_idx = 0; bin_idx < numSlaveBins; bin_idx++) {
					test_and_set_flag(&(task4bins2[0][bin_idx].config_phS[slot]));
				}
				/* Wait for Shuffling Phase to finish */
				for(bin_idx = 0; bin_idx < numSlaveBins; bin_idx++) {
					spin_on_flag(&(task4bins2[0][bin_idx].phS_done[slot]),0);
				}


				for(bin_idx = 0; bin_idx < numSlaveBins; bin_idx++) {
					test_and_set_flag(&(task4bins2[2][bin_idx].config_ph3[slot]));
				}
				/* Wait for ANTCOMB tasks to end */
				for(bin_idx = 0; bin_idx < numSlaveBins; bin_idx++) {
					spin_on_flag(&(task4bins2[2][bin_idx].ph3_done[slot]),0);
				}
			}

			for(bin_idx = 0; bin_idx < numSlaveBins; bin_idx++) {
				test_and_set_flag(&(task4bins2[0][bin_idx].config_intrlv));
			}
			/* Wait for DEMAP tasks to end */
			for(bin_idx = 0; bin_idx < numSlaveBins; bin_idx++) {
				spin_on_flag(&(task4bins2[0][bin_idx].intrlv_done),0);
			}

			for(bin_idx = 0; bin_idx < numSlaveBins; bin_idx++) {
				test_and_set_flag(&(task4bins2[3][bin_idx].config_ph4));
			}
			/* Wait for DEMAP tasks to end */
			for(bin_idx = 0; bin_idx < numSlaveBins; bin_idx++) {
				spin_on_flag(&(task4bins2[3][bin_idx].ph4_done),0);
			}

			for(bin_idx = 0; bin_idx < numSlaveBins; bin_idx++) {
				test_and_set_flag(&(task4bins2[0][bin_idx].config_crcturbo));
			}
			/* Wait for Shuffling Phase to finish */
			for(bin_idx = 0; bin_idx < numSlaveBins; bin_idx++) {
				spin_on_flag(&(task4bins2[0][bin_idx].crcturbo_done),0);
			}
		}
		clock_gettime(CLOCK_REALTIME,&sf_stop);
		
		// fprintf(ueetGroundTruthFD2,"%d,%f,%f,%f\n",\
		// subframe_count,\
		// calculate_time_diff_spec(sf_start,orig)/1000000.0,\
		// calculate_time_diff_spec(sf_mid,orig)/1000000.0,\
		// calculate_time_diff_spec(sf_stop,orig)/1000000.0);
		
		/* Dump the response time and execution time */
		sfet = (nmbUsers > 0) ? (calculate_time_diff_spec(sf_stop,orig)/1000000.0 - calculate_time_diff_spec(sf_mid,orig)/1000000.0):0;
		schedComp = (calculate_time_diff_spec(sf_mid,orig)/1000000.0) - (calculate_time_diff_spec(sf_start,orig)/1000000.0) ;
		rel_subframe_start = MAX7(sfrtPrev-1,0.0);
		sfrt = (nmbUsers > 0) ? rel_subframe_start+sfet : 0;
		subframe_arrival = (double)subframe_count;
		for (userIndex = 0; userIndex < nmbUsers; userIndex++) {
			fprintf(ueetGroundTruthFD2,"ue_%d_%d_%d.csv,%d,%d,%d,%f,%f,%f,%f\n",\
				mue[userIndex].seqid,mue[userIndex].bsid,mue[userIndex].crnti,\
				mue[userIndex].subframe,mue[userIndex].typ,mue[userIndex].replicationId,\
				schedComp,\
				subframe_arrival,subframe_arrival+rel_subframe_start,subframe_arrival+sfrt);
		}
		sfrtPrev = MIN7(sfrt,2.5);

		if (subframe_count % NOTIFYINTERVAL == 0)
			PRINTTOPO("bpmac_simd@%s:Completed Subframe:%d",traceFileName,subframe_count);
		subframe_count++;	/* Update the subframes */
		// PRINTTOPO("Completed Subframe2:%d",subframe_count);
	}
	PRINTTOPO("-------------END Generating Subframes---------------");
	deinit();
	clock_gettime(CLOCK_REALTIME,&bench_stop);
	#ifdef INSTRUMENTUE
	#ifndef QUIET
	PRINTTOPO("All Subframes dumped, elapsed time : %fs",calculate_time_diff_spec(bench_stop,orig)/1000000000.0);
	#endif
	fclose(ueetGroundTruthFD2);
	#endif
}