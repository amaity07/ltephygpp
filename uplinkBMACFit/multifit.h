#include <stdio.h>
#include <stdlib.h>
#include <stdatomic.h>
#include <stdint.h>
#include <assert.h>
#include "kernel_def.h"
#include "common_utils.h"
#include "util.h"

#ifndef KNOCK_OFF_COMPUTATION_BPMAC
#define MFIT_ITER			 20
#define MAX_TASKS2			 2560         /* Total Number of tasks actually created in a phase */
#define MAX_TASKS_PER_THREAD MAX_TASKS2
#define MAX_NUM_BINS 			 MAX_THREADS /* Same as number of threads */
/**
 * Task element, containing the placeholder
 * values of the global input_array and phy_vars
 * and ueids, required for appropriate computation
 * of the phy kernels
 */
 typedef struct __task4 {
	/* Meta info */
  	int 			nmbLayer;
	int 			startSc;
	int 			nmbSc;
	mod_type 		mod;
	task_type_t 	task_type;

	/* ID values */
	int 	subframe;
	int 	userid;
	int 	taskid;
	int 	slotid;

	/* Release Time */

} mfit_task4_t;

/**
 * A task bin structure is a container containing an ordered set of 
 * task indices. The first "curr_idx" of these indices are useful, rest
 * contain garbage values.
 * assigned to it by the MULTIFIT scheduler. Each thread/core is assigned
 * a unique bin. There is one extra slack bin of infinite capacity that can
 * absorb any task assigned to it. This bin is not owned to any thread/core
 * but exists for theoretical reasons.
 */
typedef struct __task_bin {
	uint16_t task4set_indices[MAX_TASKS_PER_THREAD];
	uint16_t curr_idx;  /* Current index where a new incoming task can be filled */
	uint16_t my_idx;    /* Used for various book-keeping operations */
	bool ht_enable;

	/* Maximum UEs */
	uint16_t nmbUsers;

	/* 
	 * Total number of CPUs (Workers + Master)x(Number of Processes) 
	 * reserved for the application. Keep in mind that an application
	 * can consist of multiple processes, each containing multiple 
     * thread.
     */
	unsigned int total_app_cpus;

	/* Affinity Offset */
	int aff_offset;

	/* Synchronization Variables */
	lte_atomic_t config_ph1[NMB_SLOT];
	lte_atomic_t ph1_done[NMB_SLOT];

	lte_atomic_t config_ph2[NMB_SLOT];
	lte_atomic_t ph2_done[NMB_SLOT];

	lte_atomic_t config_phS[NMB_SLOT];
	lte_atomic_t phS_done[NMB_SLOT];

	lte_atomic_t config_ph3[NMB_SLOT];
	lte_atomic_t ph3_done[NMB_SLOT];

	lte_atomic_t config_ph4;
	lte_atomic_t ph4_done;

	lte_atomic_t config_intrlv;
	lte_atomic_t intrlv_done;

	lte_atomic_t crcturbo_done;
	lte_atomic_t config_crcturbo;
} mfit_task4_bin_t;

/* initialize the task4 bin array */
void task4bin_init(mfit_task4_bin_t *bin_list, \
                   int numbins,                \
				   float *task4bin_weights,    \
                   bool atomic_init,           \
				   uint8_t nmbUsers);

float MULTIFIT2(float *task4set_weight, int n, int numbins, \
               int iterations, float *task4bin_weights,     \
               mfit_task4_bin_t *binp);

float MULTIFIT(float *task4set_weight, 	\
			   int n, int numbins, 		\
			   int iterations, 			\
			   float *task4bin_weights, \
			   int subframe, int userid,\
			   int phase,    int slot);


/* Display functions for debugging */
void print_task4(FILE *fd, \
                        const mfit_task4_t *task4);

void print_bin(FILE *fd, const mfit_task4_bin_t *bin,\
                      const mfit_task4_t *task4set);

inline void print_task4SetW(FILE *fd, const float *task4set_weight, int n);

void compute_task4_weight(mfit_task4_t *task4set, int n, float *task4set_weight);

inline void create_task4set(ue_t *uep, task_type_t task4type, \
mfit_task4_t *task4set, int task4set_idx, int slot);

/* 
 * Optimization (Execution-Time): Allocate
 * separate bins, one for each phase, This
 * decouples the schedule-computation and 
 * sf-execution.
 * ---------------------------------------
 * Phase-ID Values
 * 0 - MICF, (Shuffling, CRC+Turbo)
 * 1 - COMBWC
 * 2 - ANTCOMB
 * 3 - DEMAP
 * ---------------------------------------
 */
mfit_task4_t 	 task4set2[4][MAX_TASKS2];
mfit_task4_bin_t task4bins2[4][MAX_NUM_BINS];          /* One for each phase */

/* 
 * The weights of each of the bin is contained
 * in task4bin_weight. Similarly the weights of
 * all the task4 is computed and stored in task4set_weight2
 * It is used only by the FFD and MULTIFIT algorithm to 
 * allocate the tasks and serves
 * no other purpose. There has to be memory
 * for much large number of (bin) weights than there
 * are bins actually available to accomdate the hypothetical
 * cases where a task4 doesn't fit in a particular bin.
 */
float 			 task4set_weight2[4][MAX_TASKS2];
float			 task4bin_weights2[4][MAX_TASKS2]; /*  */


/*
 * The following data structures are used in
 * a setup where schedule computation and subframe
 * processing happen concurrently.
 */
#define STORE_SCHEDULE_Q_DEPTH 5
typedef struct __schedule_value {
	mfit_task4_t 	 task4set3[4][MAX_TASKS2];
	mfit_task4_bin_t task4bins3[4][MAX_NUM_BINS];        /* One for each phase */
	lte_atomic_t		 valid;
	lte_atomic_t		 free;
} lte_schedule_t;


/* Storage for a set of subframes that are yet to arrive */
lte_schedule_t lte_sf_schedule[STORE_SCHEDULE_Q_DEPTH];
input_data_t   lte_sf_input_data[STORE_SCHEDULE_Q_DEPTH];
ue_t           lte_sf_uelist[STORE_SCHEDULE_Q_DEPTH][MAX_USERS];
/* A flag set by the schedule computation thread */
lte_atomic_t    start_sf_processing;

#endif /* KNOCK_OFF_COMPUTATION_BPMAC */
