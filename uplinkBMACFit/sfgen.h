#ifndef SFGEN_DEF_H
#define SFGEN_DEF_H

/* Subframe Generators and Trace Parsesrs */
#include <stdatomic.h>
#include <stdint.h>
#include "kernel_def.h"
#include "util.h"

/* For initializing input data */
void init_input_data() ;

/* TRACE file */
FILE *trc;
void init_trc_file(const char *trcfile);
int gen_sf3(ue_t *mue,\
			 int size, \
			 int version);
void gen_sf3_test();


/* Defines and global variables */
#define INSTRUMENTUE
#define AFFINE_MIGRATION_SPIN 1000
#ifdef INSTRUMENTUE 
/* Performance Data files (CSV generated file) */
FILE* ueetGroundTruthFD2;
#define PDCBUFSIZE BUFSIZ*BUFSIZ
char ueetGroundTruthFD2buf[PDCBUFSIZE];
#endif

#endif