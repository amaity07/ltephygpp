#ifndef  SFET_GEN_DEF_H
#define SFET_GEN_DEF_H

int phy_G_multifit(int num_cpus,\
               const char *traceFileName,\
			   const char *dumpFileName,\
			   int version);
#endif /* SFET_GEN_DEF_H */
