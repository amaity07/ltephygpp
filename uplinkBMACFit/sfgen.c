#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <sched.h>
#include <assert.h>
#include <fcntl.h>
#include <sys/syscall.h>
#include <stdatomic.h>
#include "kernel_def.h"
#include "common_utils.h"
#include "sfgen.h"
#include "util.h"
#include <ecotools/errordefs.h>
#include <ecotools/roi_hooks.h>
#include <ecotools/cpu_uarch.h>

#define MAX 10 /* The number of subframes in frame */
//#define DEBUG_GENSF4

/* Randomly generate input data */
void init_input_data() {
	int slot, symb, sc, layer, rx, subframe;
	int i;
	unsigned short int inc = 0;
	unsigned short int dec = 16500;

	for (subframe = 0; subframe < NMB_DATA_FRAMES; subframe++) {
		for(slot = 0; slot < NMB_SLOT; slot++) {
			for(sc = 0; sc < MAX_SC; sc++) {
				for(symb = 0; symb < OFDM_IN_SLOT; symb++) {
					for(rx = 0; rx < RX_ANT; rx++) {
						pregen_data_array[subframe].in_data[slot][symb][rx][sc].re = rand();
						pregen_data_array[subframe].in_data[slot][symb][rx][sc].im = rand();
					}
				}

				for(layer = 0; layer < MAX_LAYERS; layer++) {
					pregen_data_array[subframe].in_rs[slot][sc][layer].re = dec--;
					pregen_data_array[subframe].in_rs[slot][sc][layer].im = inc++;
				}

				pregen_data_array[subframe].fftw[slot][sc].re = rand();
				pregen_data_array[subframe].fftw[slot][sc].im = rand();
			}
		}
	}
}

void init_trc_file(const char *filename) {
	trc = fopen(filename,"r"); /* check the file format */
	if(!trc) {
		PRINTERROR("Error opening trace file : %s",filename);
	}
	#ifndef QUIET
	PRINTTOPO("Trace File %s initialized",filename);
	#endif
}

/* 
 * parse an ns3 trace and generate subframes from 
 * 1. returns the number of ue in the subframe
 *    -1 if EOF
 * 2. ue information in mue
 */
int gen_sf3(ue_t *mue,\
            int max_size,\
			int version) {
	assert(max_size >= 10);
	int num_users = 0, max_users = 0;
	int layers;
	int userid;
	static int subframeId;
	int startRB = 0;
	int totalRBs = 0, totalUE = 0;
	int prbs;
	mod_t mod;
	int subframe = 0;
	int bs=-1, crnti=-1, typ=-1, isNew=-1, seqid=-1, isLast=-1;
	int replicationId=-1;

	if(trc) {
		fscanf(trc,"%d\n",&num_users);
		userid = 0;
		max_users = num_users;
		
		while(num_users--) {
			switch (version) {
				case 1 : fscanf(trc,"%d %d %d\n",&prbs,&layers,&mod); break;
				case 2 : fscanf(trc,"%d %d %d %d %d %d %d %d\n",&seqid,&bs,&crnti,&prbs,&layers,&mod,&typ,&replicationId); break;
				default : fscanf(trc,"%d %d %d\n",&prbs,&layers,&mod); break;
			}
			mue[userid].seqid = seqid;
			mue[userid].crnti = crnti;
			mue[userid].bsid = bs;
			mue[userid].replicationId = replicationId;
			mue[userid].typ = typ;
			mue[userid].subframe  = subframeId;
			mue[userid].ueid      = userid;
			mue[userid].start_rb  = 0;
			mue[userid].nmb_rb    = prbs;
			// mue[userid].mod       = mod;
			switch (mod) {
				case 1 : mue[userid].mod = MOD_QPSK;
						 break;
				case 2 : mue[userid].mod = MOD_16QAM;
						 break;
				case 3 : mue[userid].mod = MOD_64QAM;
						 break;
				case 4 : mue[userid].mod = MOD_64QAM;
						 break;
				default : mue[userid].mod = MOD_QPSK;
			}
			mue[userid].nmb_layer = layers;
			mue[userid].data	  = &(pregen_data_array[subframe%NMB_DATA_FRAMES]);
			mue[userid].last	  = (userid == max_users-1)?true:false;

			userid++;
			startRB += prbs;
			totalRBs += prbs;
			totalUE++;
		}
		if (fscanf(trc,"---------- %d\n",&subframe) == EOF) {
			fclose(trc);
			return -1;
		}
		/* Error Checks */
		if (version == 2) {
			if (subframeId != (subframe)) {
				PRINTERROR("version@%d|subframeId:%d not properly aligned to trace:%d",version,subframeId,subframe);
			}
		} else if (version == 1) {
			if (subframeId != (subframe-1)) {
				PRINTERROR("version@%d|subframeId:%d not properly aligned to trace:%d",version,subframeId,subframe);
			}
		}
		if (totalRBs > MAX_RB*MAX_BS) {
			PRINTERROR("subframeId:%d contains %d PRBs higher than %d supported",subframeId,totalRBs,MAX_RB*MAX_BS);
		}
		if (totalUE > MAX_USERS*MAX_BS) {
			PRINTERROR("subframeId:%d contains %d UEs higher than %d supported",subframeId,totalUE,MAX_USERS*MAX_BS);
		}
		// PRINTTOPO("subframe:%d,subframId(Estimated):%d\n",subframe,subframeId);
		subframeId++;
	}
	else {
		PRINTERROR("trc is NULL, reading failed\n");
	}
	return max_users;
}