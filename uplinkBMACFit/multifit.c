#include <stdio.h>
#include <stdlib.h>
#include <stdatomic.h>
#include <stdint.h>
#include <assert.h>
#include <ecotools/errordefs.h>
#include <ecotools/roi_hooks.h>
#include <ecotools/cpu_uarch.h>
#include "kernel_def.h"
#include "common_utils.h"
#include "util.h"
#include "multifit.h"

#ifndef KNOCK_OFF_COMPUTATION_BPMAC
//#define DEBUG_MFIT
/* return the expected execution time of a task */
static inline float get_expected_et(const mfit_task4_t *task) {
	float expected_et = 0.0;
	assert(task->nmbSc % 12 == 0);
	int x = task->nmbSc / 12;
	switch(task->task_type) {
		case MICF 	 : expected_et = 0.19*x + 22.0; break;
		//case COMBWC	 : expected_et = 2.136*x + 21.79;	/* with Vanchi's Optimization (Tiling = 24 for phase 2) */
		case COMBWC  : expected_et = 5.32*x + 8.1; break;
		case ANTCOMB : expected_et = 0.24*x + 10.5; break;
		case DEMAP 	 : expected_et = 1.49*x + 0.49; break;
		case SERIAL  : expected_et = 1.68*x + 1.70; break;
		default		 : {
						fprintf(stderr,"Bad task Type, exiting !!");
					   	exit(EXIT_FAILURE);
					   }
	}
	return expected_et;
}

/* Map function that computes the expected et of each tasks */
inline void compute_task4_weight(mfit_task4_t *task4set, int n, float *task4set_weight) {
	int i;
	for(i = 0; i < n; i++)
		task4set_weight[i] = get_expected_et(&task4set[i]);
}


/* Helper Functions to display tasks */
void print_task4(FILE *fd, const mfit_task4_t *task4) {
	const char *ttype;
	switch(task4->task_type) {
		case MICF    : ttype = "MICF"; break;
		case COMBWC  : ttype = "COMBWC"; break;
		case ANTCOMB : ttype = "ANTCOMB"; break;
		case DEMAP   : ttype = "DEMAP"; break;
		default		 : fprintf(stderr,"Task not supported \n");
	}
	fprintf(fd,"id:{sf:%d,ue:%d,slot:%d,taskid:%d},type:%s,wcet:%f,nmbLayer:%d\n",\
			task4->subframe, task4->userid,task4->slotid, task4->taskid,\
			ttype,\
			get_expected_et(task4),task4->nmbLayer);
}

/* Print the weight of all tasks in task4setW */
inline void print_task4SetW(FILE *fd, const float *task4set_weight, int n) {
	int i;
	fprintf(fd,"<");
	for(i = 0; i < n; i++)
		fprintf(fd,"%f,",task4set_weight[i]);
	fprintf(fd,">");
}

/* Helper Functions to generate task4set of size n of task_type*/
static inline void gen_task4set(mfit_task4_t *task4set, int n, task_type_t task_type) {
	int i;
	int prb;
	for(i = 0; i < n; i++) {
		//prb = rand()%100 + 1;
		prb = 100;
		task4set[i].nmbLayer 	= 4;
		task4set[i].nmbSc    	= prb * 12;
		task4set[i].startSc  	= 0;
		task4set[i].mod		 	= MOD_64QAM;
		task4set[i].task_type 	= task_type;

		task4set[i].subframe    = i;
		task4set[i].userid		= 9;
		task4set[i].taskid		= 5;
		task4set[i].slotid		= 1;
	}
}

/* Compute the total weight of the taskset */
static inline float compute_task4set_weight(const float *task4set_weight, \
                                            int n) {
	int i;
	float total_weight = 0.0;
	for(i = 0; i < n; i++)
		total_weight += task4set_weight[i];
	return total_weight;
}

/* Compare Two task4 */
int cmp_task4(const void *a, const void *b) {
	const mfit_task4_t *at = (mfit_task4_t*)a;
	const mfit_task4_t *bt = (mfit_task4_t*)b;

	if(get_expected_et(at) == get_expected_et(bt))
		return 0;
	else if (get_expected_et(at) < get_expected_et(bt))
		return -1;
	else
		return 1;
}

/* Helper function to display the contents of a bin */
inline void print_bin(FILE *fd, const mfit_task4_bin_t *bin,\
const mfit_task4_t *task4set) {
	int i;
	fprintf(fd,"--------Bin Contents(%d)--------\n",bin->my_idx);
	for(i = 0; i < bin->curr_idx; i++) {
		print_task4(fd,&task4set[bin->task4set_indices[i]]);
		fprintf(fd,",nmbUser:%d\n",bin->nmbUsers);
	}
	fprintf(fd,"----------------------------\n");
}

/* Initialize the task4_bins */
void task4bin_init(mfit_task4_bin_t *bin_list, int numbins,\
float *task4bin_weights, bool sync_init, uint8_t nmbUsers) {
	int i, j, slot;
	for (i = 0; i < numbins; i++) {
		for (j = 0; j < MAX_TASKS_PER_THREAD; j++)
			bin_list[i].task4set_indices[j] = -1;
		bin_list[i].curr_idx = 0;
		bin_list[i].my_idx   = i;
		bin_list[i].nmbUsers = nmbUsers;
		/* Initialize the atomic variable */
		if(sync_init) {
			for(slot = 0; slot < NMB_SLOT; slot++) {
				atomic_init(&(bin_list[i].config_ph1[slot]),false);
				atomic_init(&(bin_list[i].config_ph2[slot]),false);
				atomic_init(&(bin_list[i].config_phS[slot]),false);
				atomic_init(&(bin_list[i].config_ph3[slot]),false);
				atomic_init(&(bin_list[i].ph1_done[slot]),false);
				atomic_init(&(bin_list[i].ph2_done[slot]),false);
				atomic_init(&(bin_list[i].phS_done[slot]),false);
				atomic_init(&(bin_list[i].ph3_done[slot]),false);
			}
			atomic_init(&(bin_list[i].config_ph4),false);
			atomic_init(&(bin_list[i].config_crcturbo),false);
			atomic_init(&(bin_list[i].ph4_done),false);
			atomic_init(&(bin_list[i].crcturbo_done),false);
		}
	}
	for(i = 0; i < MAX_TASKS2; i++)
		task4bin_weights[i] = 0.0;

	//printf("All Bins Initialized \n");
}

/* 
 * Insert a task4 to a bin 
 * -----------------------
 * Accepts :-
 * 1. An index of task4set
 * 2. Pointer to the Bin, 
 * where the task4set is to be inserted
 */
float insert_mfit_task4_2_bin(mfit_task4_bin_t *B, uint32_t task4set_idx) {
	B->task4set_indices[B->curr_idx++]  = task4set_idx;
}

/**
 * Find the lowest bin index that can accomodate the given 
 * task ("task4" in this case). 
 * It is assumed that infinite bins are available and
 */
static inline int find_bin_idx(float task4_weight, float C, \
float *task4bin_weights, \
int subframe, int userid, int phase, int slot, int j) {
	int i = 0;
	//assert(task4_weight <= C);
	if(task4_weight > C) {
		fprintf(stderr,"task4_weight = %f, C = %f for {%d|%d|%d|%d|%d}\n",\
		task4_weight,C,subframe,userid,phase,slot,j);
		exit(EXIT_FAILURE);
	}
	for(i = 0; i < MAX_TASKS2; i++) {
#ifdef DEBUG_MFIT
	fprintf(stdout,"bin(%d)_weight = %f, task4_weight = %f, Capacity = %f\n",\
	i,task4bin_weights[i],task4_weight,C);
#endif
		if(task4_weight + task4bin_weights[i] <= C) {
			/* Insert the task to the bin */
			task4bin_weights[i] += task4_weight;
			return i;
		}
	}
	fprintf(stderr,"Capacity overflow, not enough bins\n");
	exit(EXIT_FAILURE);
}

/* A sanitized implementation of the above */
static inline int find_bin_idx2(float task4_weight, float C, \
                                float *task4bin_weights) {
	int i = 0;
	assert(task4_weight <= C);
	for(i = 0; i < MAX_TASKS2; i++) {
		if(task4_weight + task4bin_weights[i] <= C) {
			/* Insert the task to the bin */
			task4bin_weights[i] += task4_weight;
			return i;
		}
	}	
	fprintf(stderr,"Capacity overflow, not enough bins\n");
	exit(EXIT_FAILURE);
}

/* 
 * First Fit Decreasing - A bin packing heuristic which 
 * accepts a taskset weights (of size n) and a bin capacity 
 * (C) and returns
 * the number of bins required
 * Assume the task4set is already sorted in ascending order,
 * unsorted taskset is still functionally correct but suboptimal.
 * Also it might violate the monotonicity requirements of the
 * MULTIFIT algorithm.
 * MULTIFIT will sort the taskset or it will ensure that only
 * sorted taskset are accepted.
 */
static inline int FFD(const float *task4set_weight, int n, float C, \
float *task4bin_weights, \
int subframe, int userid, int phase, int slot,\
bool allocate) {
	/* Add an (optional) assertion to check C > max(task4set)*/
	int j;

	/* Reset/Initialize the bin weights (Vectorize this if possible)*/
	for(j = 0; j < MAX_TASKS2; j++)
		task4bin_weights[j] = 0.0;

	int k = 0;
	int m = k;
	for(j = n-1; j >= 0; j--) {
		k = find_bin_idx(task4set_weight[j],C,task4bin_weights,\
		subframe,userid,phase,slot,j);
		//k = 0;
		if(k > m)
			m = k;
		/* Allocate the tasks */
		if(allocate)
			insert_mfit_task4_2_bin(&task4bins2[phase][k],j);
	}
	return m+1;
}

/* A sanitized implementation of the FFD algorithm */
static inline int FFD2(const float *task4set_weight, int n,\
					   float C, float *task4bin_weights,\
		               mfit_task4_bin_t *binp,\
                       bool allocate) {
	/* Add an (optional) assertion to check C > max(task4set)*/
	int j;

	/* Reset/Initialize the bin weights (Vectorize this if possible)*/
	for(j = 0; j < MAX_TASKS2; j++)
		task4bin_weights[j] = 0.0;
	
	int k = 0;
	int m = k;
	for(j = n-1; j >= 0; j--) {
		k = find_bin_idx2(task4set_weight[j],C,task4bin_weights);
		if(k > m)
			m = k;
		/* Allocate the tasks */
		if(allocate)
			insert_mfit_task4_2_bin(binp,j);
	}
	return m+1;
}

static void testFFD(const float *task4set_weight,int numtasks,float C) {
	int i = 0;
	/* Generate the taskset */
	//for (i = 0; i < numtasks; i++) {
	//	print_task4(stdout,&task4set[i]);
	//	printf("\n");
	//}
	float task4bin_weights[MAX_TASKS2];
	int m = FFD(task4set_weight, numtasks, C, task4bin_weights, false,0,-1,1,0);
	//printf("Number of bins required = %d\n",m);
}

//#define PROFILE_MFIT
#define MAX4(a,b) a>b?a:b

/* Compare two floats for qsort */
static int cmpfunc (const void * a, const void * b) {
	float *ad = (float *)a;
	float *bd = (float *)b;
	if (ad > bd)
		return 1;
	else if (ad == bd)
		return 0;
	else
		return -1;
}

/**
 * MULTIFIT Algorithm (uses the above) FFD heuristic
 * The execution time of MULTIFIT depends upon
 * -------------------------------------------------
 * 1. |task4set_weight| (=n)
 * 2. iterations : Number of iterations for convergence to kOPT.
 * 3. FFD(.)	 : A bin packing heuristic
 * 4. qsort(.)	 : Performance of inbuilt qsort routine
 * You can assume (for now) that the taskset is sorted
 */
float MULTIFIT(float *task4set_weight, int n, int numbins, \
int iterations, float *task4bin_weights, \
int subframe, int userid, int phase, int slot) {
 
	/* Sort the task4set (ascending order) */
	//qsort(task4set_weight, n, sizeof(float), cmpfunc); /* 1. PROFILE CHKPNT */
	
	int i, m;
	float CL[iterations+1];
	float CU[iterations+1];
	float C;

	/* 2. PROFILE CHKPNT */
	float task4_set_W = compute_task4set_weight(task4set_weight,n);
	float max_task4_W = task4set_weight[n-1]; /* The task4set is sorted in ascending order */
	CL[0] =  MAX4(task4_set_W/numbins, max_task4_W);
	CU[0] =  MAX4(task4_set_W*2/numbins, max_task4_W);
	//printf("max_task4_weight = %f, total_task4_weight = %f\n",max_task4_W,task4_set_W);
	//printf("CL[0] = %f, CU[0] = %f\n",CL[0],CU[0]);
	//printf("\n\n\n");
	
	for (i = 1; i <= iterations; i++) {
		C = (CL[i-1] + CU[i-1])/2;

		/* Get the number of bins required using FFD heuristic */
		m = FFD(task4set_weight,n,C,task4bin_weights,\
		subframe,userid,phase,slot,false);
	
		//printf("iteration=%d,C=%f,m=%d\n",i,C,m);
		if ( m <= numbins) {
			CU[i] = C;
			CL[i] = CL[i-1];
		}
		else {
			CU[i] = CU[i-1];
			CL[i] = C;
		}
	}
	
	/* (Optional) Final Allocation of capacity CU[iterations] */
	float CFinal = CU[iterations];
	m = FFD(task4set_weight,n,CFinal,task4bin_weights,\
	subframe,userid,phase,slot,true);
	
	assert(m <= numbins);
	//printf("iteration=%d,CFinal=%f,m=%d\n",i,CFinal,m);
	return CFinal;
}


/* A sanitized implementation of the above algorithm */
float MULTIFIT2(float *task4set_weight, int n, int numbins, \
               int iterations, float *task4bin_weights, \
               mfit_task4_bin_t *binp) {
	int i, m;
	float CL[iterations+1];
	float CU[iterations+1];
	float C;

	/* 2. PROFILE CHKPNT */
	float task4_set_W = compute_task4set_weight(task4set_weight,n);
	float max_task4_W = task4set_weight[n-1]; /* The task4set is sorted in ascending order */
	CL[0] =  MAX4(task4_set_W/numbins, max_task4_W);
	CU[0] =  MAX4(task4_set_W*2/numbins, max_task4_W);
	
	for (i = 1; i <= iterations; i++) {
		C = (CL[i-1] + CU[i-1])/2;

		/* Get the number of bins required using FFD heuristic */
		m = FFD2(task4set_weight,n,C,task4bin_weights,binp,false);
	
		if ( m <= numbins) {
			CU[i] = C;
			CL[i] = CL[i-1];
		}
		else {
			CU[i] = CU[i-1];
			CL[i] = C;
		}
	}
	
	/* (Optional) Final Allocation of capacity CU[iterations] */
	float CFinal = CU[iterations];
	m = FFD2(task4set_weight,n,CFinal,task4bin_weights,binp,true);
	assert(m <= numbins);
	return CFinal;
}

/* Client Testing/Code Profiling */
int mfit_test() {
	struct timespec t1, t2;
	long long t2_t1;
	int i;
	int numtasks = 120;
	float C = 200.0;
	/* Generate a Taskset */
	mfit_task4_t task4set[MAX_TASKS2];
	float task4set_weight[MAX_TASKS2];
	float task4bin_weights[MAX_TASKS2];
	gen_task4set(task4set,numtasks,COMBWC);

	clock_gettime(CLOCK_REALTIME,&t1);
	/* Compute the task4set weights */
	compute_task4_weight(task4set,numtasks,task4set_weight);
	C = MULTIFIT(task4set_weight,numtasks,MAX_NUM_BINS,4,task4bin_weights,0,-1,1,0);
	clock_gettime(CLOCK_REALTIME,&t2);
	t2_t1 = calculate_time_diff_spec(t2,t1);
	printf("MULTIFIT elapsed_time = %lld\n",t2_t1);
}

/* 
 * Create the task4sets associated with a ue
 * task4set_idx is an argument that stores the
 * first location to write to.
 */
//extern 
//inline 
void create_task4set(ue_t *uep, task_type_t task4type, \
                            mfit_task4_t *task4set,           \
                            int task4set_idx, int slot) {
	int nmbLayer  = uep->nmb_layer;
	int nmbSc	  = 12*(uep->nmb_rb);
	int startSc	  = 12*(uep->start_rb);
	int rx		  = RX_ANT;
	int numtasks  = 0;

	int i, j;
	switch(task4type) {
		case MICF 	 : numtasks = RX_ANT*nmbLayer; break;
		case COMBWC  : numtasks = MAX_COMBWC_TASKS; break;
		case ANTCOMB : numtasks = (OFDM_IN_SLOT-1)*nmbLayer; break;
		case DEMAP   : numtasks = MAX_DEMAP_TASKS; break;
		default : fprintf(stderr,"Unidentified task\n");
	}
	for(i = 0; i < numtasks; i++) {
		task4set[task4set_idx+i].nmbLayer  = nmbLayer;
		task4set[task4set_idx+i].startSc   = startSc;
		task4set[task4set_idx+i].nmbSc 	   = nmbSc;
		task4set[task4set_idx+i].mod 	   = uep->mod;
		task4set[task4set_idx+i].task_type = task4type;

		/* Set the id values */
		task4set[task4set_idx+i].subframe  = uep->subframe;
		task4set[task4set_idx+i].userid    = uep->ueid;
		task4set[task4set_idx+i].taskid    = i;
		task4set[task4set_idx+i].slotid    = slot;
		//if(task4type == COMBWC) {
		//	print_task4(stdout,&task4set[task4set_idx+i]);
		//	printf("\n");
		//}
#ifdef LOG_TASK_CREATION
		//print_task4(stdout,&task4set[task4set_idx+i]);
#endif
	}
}
#endif /* KNOCK_OFF_COMPUTATION_BPMAC */
