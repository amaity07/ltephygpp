#ifndef UTIL_DEF_H
#define UTIL_DEF_H

#include <stdatomic.h>
#include <stdint.h>
#include <stdbool.h>
#include "complex_def.h"
#include "def.h"
#include "kernel_def.h"
#include "par_limits.h"

/* HOOKS */
//#define PRINT_HOOKS
//#define UTIL_HOOKS            /* Enable the collection of Utilization Data of all the cpus */
#define NOTIFYINTERVAL             1000
#define NMB_DATA_FRAMES            300

typedef mod_type mod_t;
typedef atomic_bool lte_atomic_t;

void spin_on_flag(volatile lte_atomic_t *flg, uint32_t delta);
void test_and_set_flag(volatile lte_atomic_t *flg);

/* Input Data Structure */
typedef struct __input_data {
	ltephygpp_complex_t in_data[NMB_SLOT][OFDM_IN_SLOT][RX_ANT][MAX_SC];
	ltephygpp_complex_t in_rs[NMB_SLOT][MAX_SC][MAX_LAYERS];

	/* 
	 * Twiddle Factors : Why should it be an external input ?
	 * -----------------------------------------------------
	 * This input is currently ignored
 	 */
	ltephygpp_complex_t fftw[NMB_SLOT][MAX_SC];
} input_data_t;

/* User with its input and other meta data */
typedef struct __ue {
	/* Meta Data */
	int     frame;		/* Just to verify the correctness of Nitya's Trace */
	int 	subframe;
	int 	start_rb;
	int 	nmb_rb;
	int 	nmb_layer;
	int 	ueid;
	bool 	last;
	mod_t	mod;

	/* Actual Input */
	input_data_t *data;

	/* Check the validity */
	bool valid;

	/* ueid information */
	int seqid;
	int bsid;
	int crnti;
	int typ;
	int replicationId;
} ue_t;
int cmp_ue(const void *a, const void *b);
unsigned int print_ue(FILE *fd, const ue_t *uep);

/* Placeholder/"stack" variables for a ue computation */
typedef struct __phy_vars {
	scData_t 		layer_data[MAX_LAYERS][RX_ANT];
	int 			pow[MAX_LAYERS][RX_ANT];
	int 			res_power[MAX_LAYERS][RX_ANT];
	complexMatrix_t	R;
	complexMatrix_t comb_w[MAX_SC];
	weightSC_t		combWeight[MAX_LAYERS];
	// ltephygpp_complex_t			symbols2[NMB_SLOT][(OFDM_IN_SLOT-1)][MAX_LAYERS][MAX_SC];
	ltephygpp_complex_t			symbols[NMB_SLOT*(OFDM_IN_SLOT-1)*MAX_LAYERS*MAX_SC];
	ltephygpp_complex_t			deint_symbols[NMB_SLOT*(OFDM_IN_SLOT-1)*MAX_SC*MAX_LAYERS];
	char			softbits[2*2*(OFDM_IN_SLOT-1)*MAX_SC*MOD_64QAM*MAX_LAYERS];
	char			bits[2*(OFDM_IN_SLOT-1)*MAX_SC*MOD_64QAM*MAX_LAYERS];

	mod_t			mod;
} phy_vars_t;

typedef enum {
	MICF 	= 0,
	COMBWC	= 1,
	ANTCOMB	= 2,
	DEMAP	= 3,
	SERIAL  = 4
} task_type_t;

/* 
 * For Configuration, Written
 * by the master thread for
 * each user.
 *
 * Only WRITTEN by master thread
 * But READ by the slave threads 
 *
 * Except the LOCKS perhaps
 * But Then again "acquire" the lock using
 * trylock instead.
 * -----------------------------
 * For a 1UE case this is written
 * by the master thread
 */
typedef struct __config_task3_t {
	/* Meta info */
  	int 			nmbLayer;
	int 			startSc;
	int 			nmbSc;
	mod_type 		mod;
	task_type_t 	task_type;

	/* ID values */
	int 	subframe;
	int 	userid;
	int 	taskid;
	int 	slotid;

	/* Synchronization Variables (2 per phase) */
	volatile lte_atomic_t configured;
	volatile lte_atomic_t micf_start[NMB_SLOT];
	volatile lte_atomic_t micf_stop[NMB_SLOT];
	volatile lte_atomic_t combwc_start[NMB_SLOT];
	volatile lte_atomic_t combwc_stop[NMB_SLOT];
	volatile lte_atomic_t antcomb_start[NMB_SLOT];
	volatile lte_atomic_t antcomb_stop[NMB_SLOT];
	volatile lte_atomic_t intrlv_start;
	volatile lte_atomic_t intrlv_stop;
	volatile lte_atomic_t demap_start;
	volatile lte_atomic_t demap_stop;

} config_task3_t;

void print_task3(FILE *fd, const config_task3_t* task3);
/* ---------------------------------- Global Variables ----------------------*/
/* For Single UE case */
/* 
 * Stores UE Metadata and pointer to 
 * input Data Structure
 */
ue_t 				ue;	
input_data_t		ue_data;

/* Store the intermediate computed values of an UE */
phy_vars_t 			phy_vars;


/* Hold loop limits and parallelization configurations */
config_task3_t 		config_task_array[MAX_TASKS]; 

/* Thread Information */
pthread_t			th[MAX_THREADS];
int					th_id[MAX_THREADS];
void				*exec_phy_task_ms(void*);	/* thread start fn */

/* pregenerated Data to avoid generation time */
input_data_t		pregen_data_array[NMB_DATA_FRAMES];

#ifndef KNOCK_OFF_COMPUTATION_BPMAC
/* Additional structures to hold temporary variables */
phy_vars_t			phy_vars_mue[MAX_USERS*MAX_BS];
ue_t				mue[MAX_USERS*MAX_BS];
#endif /* KNOCK_OFF_COMPUTATION_BPMAC */

/* Affinities of the "base" cpu */
int affinity_offset;
/* --------------------------------------------------------------------------*/

void print_sf(FILE *fd, const ue_t *sf, unsigned int nmbUsers);


#endif
