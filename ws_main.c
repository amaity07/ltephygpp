/* Isolated Timing Analysis */
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <getopt.h>
#include <ctype.h>
#include <ecotools/errordefs.h>
#include "mf_4.h"
#include "fft_8.h"
#include "soft_demap_9.h"
#include "ant_comb_7.h"
#include "chest_5.h"
#include "weight_calc_6.h"
#include "uplink.h"
#include "def.h"
#include "common_utils.h"

void help() {
	PRINTTOPO("Usage: ./<prog> -m <num of cpus> -f <Trace file name> -v <version> -d <UE timing dump> -e <PH timing dump>"); 
	exit(EXIT_SUCCESS);
}

int main(int argc, char *argv[]) {
	/* Argument parsing */
	int c;
	char *fval = NULL;
	char *mval = NULL;
	char *vval = NULL;
	char *dval = NULL;
	char *eval = NULL;
	while ((c = getopt (argc, argv, "hm:f:v:d:e:")) != -1) {
		switch (c) {
			case 'm' : mval = optarg; break;
			case 'f' : fval = optarg; break;
			case 'v' : vval = optarg; break;
			case 'd' : dval = optarg; break;
			case 'e' : eval = optarg; break;
			case 'h' : help(); break;
			case '?' : {
				if (optopt == 'm' || optopt == 'f' || optopt == 'v' || optopt == 'd' || optopt == 'e')
					PRINTERROR("Option -%c requires an argument\n", optopt);
			}
		}
	}
	if (!fval) {
		help();
		PRINTERROR("Trace file not found");
	}
	if (!dval) {
		help();
		PRINTERROR("UEET file not found");
	}
	if (!eval) {
		help();
		PRINTERROR("TaskEt file not found");
	}
	int m = atoi(mval);
	int v = atoi(vval);
	uplink_ws_nosimd(m,fval,dval,eval,v);
}