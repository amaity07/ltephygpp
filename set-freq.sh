target_freq=$1
#target_freq=1500000
for id in $(seq 0 47); do
	echo userspace > /sys/devices/system/cpu/cpu${id}/cpufreq/scaling_governor
	echo ${target_freq} > /sys/devices/system/cpu/cpu${id}/cpufreq/scaling_min_freq
	echo ${target_freq} > /sys/devices/system/cpu/cpu${id}/cpufreq/scaling_setspeed
done 
