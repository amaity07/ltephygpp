#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <time.h>
#include <math.h>
#include <fcntl.h>
#include <sys/syscall.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>
#include <ctype.h>
#include <ecotools/errordefs.h>
#include <ecotools/roi_hooks.h>
#include <ecotools/cpu_uarch.h>
#include "def.h"
#include "uplink.h"
#include "uplink_parameters.h"
#include "uplink_alarm.h"
#include "common_utils.h"
#include "crc_13.h"
#include "fft_8.h"
#include "par_limits.h"

/* Test whether the User Queue is empty or not*/
bool test_uq_empty(user_queue *uq) {
	bool ret = false;
	pthread_mutex_lock(&uq->lock);
		//printf("qatatus : subframes_count = %d, qcount = %d\n",uq->subframes_count,uq->qcount);
		if(uq->subframes_count == 0 && uq->qcount == 0) {
			ret = true;
		}
	pthread_mutex_unlock(&uq->lock);
	return ret;
}


/* Initialize all the queues */
static inline void queues_init(queues* q) {
	int i;
	
	q->users.first 			 = NULL;
	q->users.last  			 = NULL;
	q->users.qcount 		 = 0;
	q->users.subframes_count = 0;
	pthread_mutex_init(&(q->users.lock),NULL);

	for(i = 0; i < MAX_THREADS; i++) {
		q->threads[i].first = NULL;
		q->threads[i].last  = NULL;
		q->threads[i].qcount = 0;
		pthread_mutex_init(&(q->threads[i].lock),NULL);
	}
}

double get_alpha(int typ) {
	switch(typ) {
		case TRAFFIC_TYPE_IOT  : return 1-1.0E-5;
		case TRAFFIC_TYPE_AVT  : return 1-1.0E-7;
		case TRAFFIC_TYPE_EMBB : return 1-1.0E-3;
		default                : return 1-1.0E-3;
	}
	return 1-1.0E-3;
}

double get_deadline(int typ) {
	switch(typ) {
		case TRAFFIC_TYPE_IOT  : return DEADLINE_IOT;
		case TRAFFIC_TYPE_AVT  : return DEADLINE_AVT;
		case TRAFFIC_TYPE_EMBB : return DEADLINE_EMBB;
		default                : return DEADLINE_EMBB;
	}
	return DEADLINE_EMBB;
}


/* Global Variable Initialization */
void init_globals(unsigned int total_app_cpus,\
                  unsigned int cpu_list[],\
				  const char *traceFileName,\
				  const char *dumpFileName,\
				  const char *phetFileName,\
				  queues *queuep,\
				  parameter_model *pmodelp) {

	int rc, i;
	
	/* Generate input data */
	#ifndef QUIET
	PRINTTOPO("Initializing Input Dataset");
	#endif
	init_data();
	crcInit();

	/* Initialize the parameter model */
	#ifndef QUIET
	PRINTTOPO("Initializing Parameter Model");
	#endif
	init_parameter_model(pmodelp);

	/* Initialize the queue */
	#ifndef QUIET
	PRINTTOPO("Initializing Queues");
	#endif
	queues_init(queuep);

	/* Initialization "performance counters */
    atomic_init(&dropsAVT,0);
	atomic_init(&dropsEMBB,0);
	atomic_init(&dropsIoT,0);
	atomic_init(&totalAVT,0);
	atomic_init(&totalEMBB,0);
	atomic_init(&totalIoT,0);
	
	/* Initialize, Traces, performance counters etc*/
	#ifndef QUIET
	PRINTTOPO("Opening PHY Trace file and performance traces");
	#endif
	init_trace_file(traceFileName);

	#ifndef KNOCK_OFF_COMPUTATION
	#ifdef INSTRUMENTUE
	ueetGroundTruthFD = fopen(dumpFileName,"w");
	setvbuf(ueetGroundTruthFD,ueetGroundTruthFDbuf,ueetGroundTruthFDbuf ? _IOFBF : _IONBF,PDCBUFSIZE);
	fprintf(ueetGroundTruthFD,"ueid,subframe,slice,replicationId,start,startProcessing,end\n");
	#endif
	#else 
	/* use dumpFileName for dumping subframes processed in IMDEA format*/
	convertToIMDEAFormatFD = fopen(dumpFileName,"w");
	if (convertToIMDEAFormatFD == NULL) {
		PRINTERROR("Error opening %s",dumpFileName);
	}
	#endif
	#ifdef WS_TIMING_ANALYSES
	phaseEtGroundTruthFD = fopen(phetFileName,"w");
	setvbuf(phaseEtGroundTruthFD,phaseEtGroundTruthFDbuf,phaseEtGroundTruthFDbuf ? _IOFBF : _IONBF,PDCBUFSIZE);
	fprintf(phaseEtGroundTruthFD,"ueid,subframe,"
								 "TaskType,TaskId,SlotId,"
								 "CoreidCreate,CoreidExec,"
								 "start,startProcessing,"
								 "stop\n");
	#endif

	#ifndef KNOCK_OFF_COMPUTATION
	/* Start a number of worker threads */
	#ifndef QUIET
	PRINTTOPO("Creating %d Daemon Threads",total_app_cpus);
	#endif
	for (i=0; i < total_app_cpus; i++) {
		/* Setup arguments */
		args[i].tid               = i;
		args[i].active            = &(pmodelp->active);
		args[i].queue             = queuep;
		args[i].ht_enable         = false;
		args[i].total_app_cpus    = total_app_cpus;
		args[i].aff               = cpu_list[i];

		/* Start thread */
		rc = pthread_create(&threads[i], NULL, uplink_task, &args[i]);
		if (rc) {
			PRINTERROR("return code from pthread_create() is %d\n", rc);
		}
	}
	#endif /* KNOCK_OFF_COMPUTATION */
}