/*******************************************************************************
 *                      LTE UPLINK RECEIVER PHY BENCHMARK                      *
 *                                                                             *
 * This file is distributed under the license terms given by LICENSE.TXT       *
 *******************************************************************************
 * Author: Magnus Sjalander                                                    *
 ******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include <ecotools/errordefs.h>
#include <ecotools/roi_hooks.h>
#include "common_utils.h"
#include "def.h"
#include "complex_def.h"
#include "kernel_def.h"
#include "uplink.h"
#include "interleave_11.h"
#include "ant_comb_7.h"
#include "mf_4.h"
#include "chest_5.h"
#include "fft_8.h"
#include "soft_demap_9.h"
#include "weight_calc_6.h"
#include "crc_13.h"
#include "turbo_dec_12.h"

bool monitorDropInstance(const userS *user) {
	return true;
}

inline int max(int a, int b) {
	return (abs(a) > abs(b))? a : b;
}

inline double get_maxdiff_comb_w(complexMatrix_t* w1, complexMatrix_t* w2) {
	int i, j, k, n = 0;
	double rms, sum = 0.0;
	long long int diff_re, diff_im;
	for(i = 0; i < MAX_SC; i++) {
		for(j = 0; j < RX_ANT; j++) {
			for(k = 0; k < MAX_LAYERS; k++) {
				diff_re = w1[i][j][k].re - w2[i][j][k].re;
				diff_im = w1[i][j][k].im - w2[i][j][k].im;
				sum += (diff_re*diff_re + diff_im* diff_im);
				n++;
			}
		}
	}
	rms = abs(sqrt(sum/n));
	return rms;
}

inline int compare_softbits(const char* a, const char* b) {
	int i;
	for(i=0;i < (2*2*(OFDM_IN_SLOT-1)*MAX_SC*MOD_64QAM*MAX_LAYERS);i++) {
		if(a[i] != b[i])
			return -1;
	}
	return 0;
}

void wait_until_computed(task *tasks, int items) {
	int i=0;
	
	while(i<items) {
	  while(tasks[i].computed==false);
	  i++;
	}
}

inline void compute_micf(task *taskp) {
	int subframe 		  = taskp->subframe;
	int userid	 		  = taskp->userid;
	int taskid	 		  = taskp->taskid;
	int nmbLayer 		  = taskp->nmbLayer;
	int startSc	 		  = taskp->startSc;
	int nmbSc	 		  = taskp->nmbSc;
	int slot			  = taskp->slotid;
	input_data *ue_data_p 	= taskp->data;
	symbol_data *phy_vars_p = taskp->symbolData;

	if(taskid < nmbLayer * RX_ANT) {
		int layer 	 = taskid / RX_ANT;
		int rx		 = taskid % RX_ANT;
		/* Matched Filter */
		// printf("id(%d|%d|%d),layer=%d,rx=%d\n",subframe,userid,taskid,layer,rx);
		mf(&(ue_data_p->in_data[slot][3][rx][startSc]), 	 \
		   &(ue_data_p->in_rs[slot][startSc][layer]), /* Check the indices*/ \
		   nmbSc, phy_vars_p->layer_data[layer][rx],  \
		   &(phy_vars_p->pow[layer][rx]));
		/* IFFT */
		ifft(phy_vars_p->layer_data[layer][rx],			\
			 nmbSc,										\
			 ue_data_p->fftw[slot]);
		/* Windowing */
		window(phy_vars_p->layer_data[layer][rx],			\
			  phy_vars_p->pow[layer][rx],					\
			  nmbSc,										\
			  phy_vars_p->layer_data[layer][rx],			\
			  &(phy_vars_p->res_power[layer][rx]));
		/* Intermediate Data Storage */
		phy_vars_p->R[layer][rx].re = 0.0;
		phy_vars_p->R[layer][rx].im = 0.0;
		/* FFT */
		fft(phy_vars_p->layer_data[layer][rx],\
			  nmbSc,ue_data_p->fftw[slot]);
	}
	/* Mark the taskp as computed */
	taskp->computed = true;
}

inline void compute_antcomb(task *taskp) {
	int subframe = taskp->subframe;
	int userid	 = taskp->userid;
	int taskid	 = taskp->taskid;
	int nmbLayer = taskp->nmbLayer;
	int nmbSc	 = taskp->nmbSc;
	int startSc	 = taskp->startSc;
	int slot	 = taskp->slotid;
	input_data *ue_data_p 	= taskp->data;
	symbol_data *phy_vars_p = taskp->symbolData;

	if(taskid < nmbLayer*(OFDM_IN_SLOT-1)) {
		ltephygpp_complex_t* in[RX_ANT];
		int index_out;
		int ofdm_count;
		int ofdm;
		int layer, rx;
		int sc;

		layer       = taskid / (OFDM_IN_SLOT-1);
		ofdm_count  = taskid % (OFDM_IN_SLOT-1);
		ofdm        = (ofdm_count > 2)?(ofdm_count+1):ofdm_count;

		/* Put all demodulated symbols in one long vector */
		for(rx = 0; rx < RX_ANT; rx++) {
			in[rx] = &ue_data_p->in_data[slot][ofdm][rx][startSc];
		}

		index_out = nmbSc*ofdm_count + slot*(OFDM_IN_SLOT-1)*nmbSc + layer*2*(OFDM_IN_SLOT-1)*nmbSc;
		ant_comb(in, phy_vars_p->combWeight[layer], nmbSc, \
				 &phy_vars_p->symbols[index_out]);
		/* Now transform data back to time plane */
		ifft(&phy_vars_p->symbols[index_out], \
			nmbSc, ue_data_p->fftw[slot]);
		/* Write to symbols variable */
		// base_idx = slot*(OFDM_IN_SLOT-1)*MAX_LAYERS*MAX_SC + \
		// 		   ofdm_count*MAX_LAYERS*MAX_SC  + \
		// 		   layer*MAX_SC + startSc;
		// for(sc = 0; sc < nmbSc; sc++)
		// phy_vars_p->symbols[base_idx+sc] = phy_vars_p->symbols2[slot][ofdm_count][layer][sc];
	}
	taskp->computed = true;
}

/* =========== compute_combwc function ==========*/
#ifdef COMB_WC_PAR
inline void compute_combwc(task *taskp) {
	int subframe = taskp->subframe;
	int userid	 = taskp->userid;
	int taskid	 = taskp->taskid;
	int nmbLayer = taskp->nmbLayer;
	int nmbSc	 = taskp->nmbSc;
	int slot	 = taskp->slotid;
	input_data *ue_data_p 	= taskp->data;
	symbol_data *phy_vars_p = taskp->symbolData;

	if(taskid < MAX_COMBWC_TASKS) {
		/* Calculate the range of subcarriers to compute on */
		// PRINTTOPO("nmbSc=%d,MAX_COMBWC_TASKS=%d",nmbSc,MAX_COMBWC_TASKS);
		assert(nmbSc % MAX_COMBWC_TASKS == 0);
		int start_tile				= (taskid)*(nmbSc/MAX_COMBWC_TASKS);
		int end_tile				= (taskid+1)*(nmbSc/MAX_COMBWC_TASKS);

		comb_w_calc_partiled_generic(phy_vars_p->layer_data,		\
							 start_tile,end_tile,nmbLayer,	\
							 phy_vars_p->R,					\
							 phy_vars_p->comb_w);
		//printf("combwc(%d|%d|%d|%d) done\n",subframe,userid,slotid,taskid);
	}
	taskp->computed = true;
}
#endif /* COMB_WC_PAR */

#ifdef DEMAP_PAR
inline void compute_demap(task *taskp) {
	int subframe = taskp->subframe;
	int userid	 = taskp->userid;
	int taskid	 = taskp->taskid;
	int nmbLayer = taskp->nmbLayer;
	int nmbSc	 = taskp->nmbSc;
	mod_type mod = taskp->mod;
	symbol_data *phy_vars_p = taskp->symbolData;

	if(taskid < MAX_DEMAP_TASKS) {
		ltephygpp_complex_t *deint_symbols			= phy_vars_p->deint_symbols;
		char *softbits					= phy_vars_p->softbits;
		int pow							= phy_vars_p->pow[0][0];

		int nmbSymbols  				= NMB_SLOT*nmbSc*(OFDM_IN_SLOT-1)*nmbLayer;
		soft_demap_pthread(deint_symbols, pow, mod, nmbSymbols, softbits, MAX_DEMAP_TASKS, taskid);
	}
	taskp->computed = true;
}
#endif


void uplink_user(task_queue *queue, userS *user, task *tasks,int tid) {
	struct timespec userExecutionStartTime;
	clock_gettime(CLOCK_MONOTONIC,&userExecutionStartTime);
	user->startProcessing = calculate_time_diff_spec(userExecutionStartTime,start_time)/1000000.0;
	#ifdef DROP_ENABLED
	struct timeval	 ph_time;
	#endif
	int subframe					= user->subframe;
	int ueid						= user->userid;
	#ifdef DROP_ENABLED
	gettimeofday(&ph_time,NULL);
	if(calculate_time_diff(ph_time,ue_start[subframe][ueid]) > DEADLINE) {
		ue_discarded[subframe][ueid] = 1;
		return;
	}
	#endif
	mod_type  		mod    		= user->mod;
	int 	  		startRB	  	= user->startRB;
	int 	  		nmbRB		= user->nmbRB;
	int 	  		nmbLayer    = user->nmbLayer;
	int 	  		startSc     = startRB*SC_PER_RB;
	int 	  		nmbSc       = nmbRB*SC_PER_RB;
	int 	  		nmbSymbols  = NMB_SLOT*nmbSc*(OFDM_IN_SLOT-1)*nmbLayer; /* 2 is for two slots in a subframe */
	int 	  		nmbSoftbits = nmbSymbols * mod;
	int 	  		layer;
	int 	  		rx;
	int 	  		ofdm;
	int 	  		slot;
	int 	  		sc;
	int 	  		i, tile;
	double 	  		rms 			= 0.0;
	int 	  		err_softbits 	= 0;

	symbol_data 	*symbolData 	= tasks[0].symbolData;
	/* Output place holders */
	complexMatrix_t *comb_w 		= symbolData->comb_w;
	#ifdef VERIFY_COMB_WC_PAR
	complexMatrix_t *comb_w1 		= symbolData->comb_w1;
	#endif
	weightSC_t 		*combWeight 	= symbolData->combWeight;
	ltephygpp_complex_t 		*symbols    	= symbolData->symbols;
	ltephygpp_complex_t 		*deint_symbols 	= symbolData->deint_symbols;
	//int				pow[RX_ANT][RX_ANT]			= symbolData->pow;
	char 			*softbits       = symbolData->softbits;
	#ifdef VERIFY_DEMAP_PAR
	char 			*softbits_pth   = symbolData->softbits_pth;
	#endif
	unsigned char 	*bits     		= symbolData->bits;

	//printf("tid@%d started\n",tid);
	for (slot=0; slot < NMB_SLOT; slot++) {
		/* START MICF */
		for(i = 0; i < RX_ANT*nmbLayer; i++) {
			tasks[i].nmbLayer = nmbLayer;
			tasks[i].startSc  = startSc;
			tasks[i].nmbSc	  = nmbSc;
			tasks[i].mod	  = mod;
			tasks[i].subframe = user->subframe;
			tasks[i].userid   = user->userid;	
			tasks[i].taskid   = i;	
			tasks[i].slotid	  = slot;
			tasks[i].type     = CHANNEL;
			tasks[i].data	  = user->data;
			#ifdef WS_TIMING_ANALYSES
			tasks[i].arrival = getCurrentTime(&start_time);
			tasks[i].seqid = user->seqid;
			tasks[i].crnti = user->crnti;
			tasks[i].bs = user->bs;
			#endif
			if (i != 0)
			  tasks[i-1].next = &tasks[i];
			/* set the configuration flags */
			tasks[i].computed = false;
		}
		tasks[i-1].next = NULL;
		/* enq_task_q : for app_phase_2 */
		pthread_mutex_lock(&(queue->lock));
		if (queue->first == NULL) {
		  queue->first = tasks;
		} else {
		  queue->last->next = tasks;
		}
		queue->last = &tasks[i-1];
		queue->qcount += i;
		pthread_mutex_unlock(&(queue->lock));
		/* process : the app_phase_2 tasks */
		while(queue->first)
			handle_task(queue,tid,tid);
		wait_until_computed(tasks, RX_ANT*nmbLayer);
		#ifdef DROP_ENABLED
		gettimeofday(&ph_time,NULL);
		if(calculate_time_diff(ph_time,ue_start[subframe][ueid]) > DEADLINE) {
			ue_discarded[subframe][ueid] = 1;
			return;
		}
		#endif





		/* START COMBWC */	
		#ifdef COMB_WC_PAR
		for(i = 0; i < MAX_COMBWC_TASKS; i++) {
			tasks[i].nmbLayer = nmbLayer;
			tasks[i].startSc  = startSc;
			tasks[i].nmbSc	  = nmbSc;
			tasks[i].mod	  = mod;
			tasks[i].subframe = user->subframe;
			tasks[i].userid   = user->userid;	
			tasks[i].taskid   = i;	
			tasks[i].slotid	  = slot;
			tasks[i].type     = COMBWCALC;
			tasks[i].data	  = user->data;
			#ifdef WS_TIMING_ANALYSES
			tasks[i].arrival = getCurrentTime(&start_time);
			tasks[i].seqid = user->seqid;
			tasks[i].crnti = user->crnti;
			tasks[i].bs = user->bs;
			#endif
			if (i != 0)
			  tasks[i-1].next = &tasks[i];
			/* Set the configuration flags */
			tasks[i].computed = false;
		}
		tasks[i-1].next = NULL;
		/* enq_task_q : COMBWC */
		pthread_mutex_lock(&(queue->lock));
		if (queue->first == NULL) {
		  queue->first = tasks;
		} else {
		  queue->last->next = tasks;
		}
		queue->last = &tasks[i-1];
		queue->qcount += i;
		pthread_mutex_unlock(&(queue->lock));
		/* process task : COMBWC  */
		while(queue->first)
			handle_task(queue,tid,tid);
		wait_until_computed(tasks, MAX_COMBWC_TASKS);
		#ifdef VERIFY_COMB_WC_PAR
		comb_w_calc(symbolData->layer_data, nmbSc, nmbLayer, symbolData->R, comb_w1);
		rms += get_maxdiff_comb_w(comb_w, comb_w1);
		if(rms > 0.0001) {
			fprintf(stderr,"subframe:%d,user:%d,comb weight parallelization failed err:%d\n",user->subframe,user->userid,rms);
			exit(EXIT_FAILURE);
		}
		#endif /* VERIFY_COMB_WC_PAR */
		#else
		comb_w_calc(symbolData->layer_data, nmbSc, nmbLayer, symbolData->R, comb_w);
		#endif /* COMB_WC_PAR */
		#ifdef DROP_ENABLED
		gettimeofday(&ph_time,NULL);
		if(calculate_time_diff(ph_time,ue_start[subframe][ueid]) > DEADLINE) {
			ue_discarded[subframe][ueid] = 1;
			return;
		}
		#endif






		/* SHUFFLING */
		for (rx=0; rx<RX_ANT; rx++)
		  for (layer=0; layer<user->nmbLayer; layer++)
		    for (sc=0; sc<nmbSc; sc++)
		      combWeight[layer][sc][rx] = comb_w[sc][layer][rx];
		#ifdef DROP_ENABLED
		gettimeofday(&ph_time,NULL);
		if(calculate_time_diff(ph_time,ue_start[subframe][ueid]) > DEADLINE) {
			ue_discarded[subframe][ueid] = 1;
			return;
		}
		#endif









		/* ANTCOMB */	
		for(i = 0; i < (OFDM_IN_SLOT-1)*nmbLayer; i++) {
			tasks[i].nmbLayer = nmbLayer;
			tasks[i].startSc  = startSc;
			tasks[i].nmbSc	  = nmbSc;
			tasks[i].mod	  = mod;
			tasks[i].subframe = user->subframe;
			tasks[i].userid   = user->userid;	
			tasks[i].taskid   = i;	
			tasks[i].slotid	  = slot;
			tasks[i].type     = SYMBOL;
			tasks[i].data	  = user->data;
			#ifdef WS_TIMING_ANALYSES
			tasks[i].arrival = getCurrentTime(&start_time);
			tasks[i].seqid = user->seqid;
			tasks[i].crnti = user->crnti;
			tasks[i].bs = user->bs;
			#endif
			if (i != 0)
			  tasks[i-1].next = &tasks[i];
			/* Set the configuration flags */
			tasks[i].computed = false;
		}
		tasks[i-1].next = NULL;
		pthread_mutex_lock(&(queue->lock));
		if (queue->first == NULL) {
		  queue->first = tasks;
		} else {
		  queue->last->next = tasks;
		}
		queue->last = &tasks[i-1];
		queue->qcount += i;
		pthread_mutex_unlock(&(queue->lock));
		while(queue->first)
			handle_task(queue,tid,tid);
		wait_until_computed(tasks, i);
		#ifdef DROP_ENABLED
		gettimeofday(&ph_time,NULL);
		if(calculate_time_diff(ph_time,ue_start[subframe][ueid]) > DEADLINE) {
			ue_discarded[subframe][ueid] = 1;
			return;
		}
		#endif
	} /* slot loop */




	/* INTERLEAVE */
	//uplink_symbol_verify(user->subframe, symbols, nmbSymbols);
	interleave(symbols, deint_symbols, nmbSymbols);
	//uplink_interleave_verify(user->subframe, deint_symbols, nmbSymbols);




	/* DEMAP */
	#ifdef DEMAP_PAR	
	for(i = 0; i < MAX_DEMAP_TASKS; i++) {
			tasks[i].nmbLayer = nmbLayer;
			tasks[i].startSc  = startSc;
			tasks[i].nmbSc	  = nmbSc;
			tasks[i].mod	  = mod;
			tasks[i].subframe = user->subframe;
			tasks[i].userid   = user->userid;	
			tasks[i].taskid   = i;	
			tasks[i].slotid	  = slot;
			tasks[i].type     = DEMAP;
			tasks[i].data	  = user->data;
			#ifdef WS_TIMING_ANALYSES
			tasks[i].arrival = getCurrentTime(&start_time);
			tasks[i].seqid = user->seqid;
			tasks[i].crnti = user->crnti;
			tasks[i].bs = user->bs;
			#endif
			if (i != 0)
			  tasks[i-1].next = &tasks[i];
			/* Set the configuration flags */
			tasks[i].computed = false;
	}
  	pthread_mutex_lock(&(queue->lock));
	if (queue->first == NULL) {
		queue->first = tasks;
	} else {
		queue->last->next = tasks;
	}
	queue->last = &tasks[i-1];
	queue->qcount += i;
	pthread_mutex_unlock(&(queue->lock));
    while(queue->first)
		handle_task(queue,tid,tid);
	wait_until_computed(tasks, MAX_DEMAP_TASKS);
	#ifdef VERIFY_DEMAP_PAR
	printf("Demap parallelization verification entered...\n");
	soft_demap(deint_symbols, pow[0], mod, nmbSymbols, softbits_pth);
	err_softbits = compare_softbits(softbits,softbits_pth);
	if(err_softbits < 0) {
		fprintf(stderr,"subframe:%d,user:%d,demap parallelization failed err:%d\n",\
		user->subframe,user->userid,err_softbits);
		exit(EXIT_FAILURE);
	}
	#endif /* VERIFY_DEMAP_PAR */
	#else
	soft_demap(deint_symbols, symbolData->pow[0][0], mod, nmbSymbols, softbits);
	#endif /* DEMAP_PAR */
	/* End the time app_phase_4 */
	#ifdef DROP_ENABLED
	gettimeofday(&ph_time,NULL);
	if(calculate_time_diff(ph_time,ue_start[subframe][ueid]) > DEADLINE) {
		ue_discarded[subframe][ueid] = 1;
		return;
	}
	#endif



	
	/* turbo decoder and CRC check*/
  	turbo_dec(nmbSoftbits);
  	crcFast(bits, nmbSoftbits/24);

	user->computed = true;
	
	// gettimeofday(&(user->end),NULL);
	user->end = getCurrentTime(&start_time);
	#ifdef CACHE_WARMUP_SUBFRAMES
	bool enableDropEstimation = ((user->subframe >= CACHE_WARMUP_SUBFRAMES) ? true : false) && (monitorDropInstance(user)) ;
	#else
	bool enableDropEstimation = true;
	#endif
	double ueet = user->end - user->start;
	double ueetOnlyExec = user->end - user->startProcessing;
	switch(user->typ) {
		case TRAFFIC_TYPE_IOT  : 
			if ((ueet > DEADLINE_IOT) && enableDropEstimation)
			{
				atomic_fetch_add(&dropsIoT,1);
			}
			break;
		case TRAFFIC_TYPE_AVT  : 
			if ((ueet > DEADLINE_AVT) && enableDropEstimation) {
				atomic_fetch_add(&dropsAVT,1);
			}
			break;
		case TRAFFIC_TYPE_EMBB : 
			if ((ueet > DEADLINE_EMBB) && enableDropEstimation) {
				atomic_fetch_add(&dropsEMBB,1);
			}
			break;
		default                : 
			if ((ueet > DEADLINE_EMBB) && (subframe >= CACHE_WARMUP_SUBFRAMES)) atomic_fetch_add(&dropsEMBB,1);
			break;
	}
	// print_ue(stdout,user,ueetOnlyExec);
}


uint64_t compute_wkld_vol(const userS *user) {
	int mod;
	switch (user->mod) {
		case MOD_QPSK : mod = 2; break;
		case MOD_16QAM : mod = 4; break;
		case MOD_64QAM : mod = 6; break;
		case MOD_256QAM : mod = 8; break;
	}
	uint64_t volume = mod * user->nmbRB * user->nmbLayer;
	return volume;
}