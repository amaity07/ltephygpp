/*******************************************************************************
 *                      LTE UPLINK RECEIVER PHY BENCHMARK                      *
 *                                                                             *
 * This file is distributed under the license terms given by LICENSE.TXT       *
 *******************************************************************************
 * Author: Magnus Sjalander                                                    *
 ******************************************************************************/

#ifndef _UPLINK_H
#define _UPLINK_H

#include <stdio.h>
#include <sys/time.h>
#include <time.h>
#include <assert.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdatomic.h>
#include "def.h"
#include "kernel_def.h"
#include "par_limits.h"

typedef enum {
	CHANNEL 	= 0,
	SYMBOL  	= 1,
	COMBWCALC 	= 2,
	DEMAP		= 3
} task_type;

typedef struct {
	ltephygpp_complex_t in_data[NMB_SLOT][OFDM_IN_SLOT][RX_ANT][MAX_SC];
	ltephygpp_complex_t in_rs[NMB_SLOT][MAX_SC][MAX_LAYERS];
	ltephygpp_complex_t fftw[NMB_SLOT][MAX_SC];
} input_data;

typedef struct userS userS;

struct userS {
	/* Workload */
	int 		nmbLayer;
	int 		startRB;
	int 		nmbRB;
	mod_type 	mod;
	userS 		*next;
	input_data 	*data;
	
	/* Identifiers and book keeping */
	int seqid;
	int  bs;
	int  crnti;
	int  subframe;
	int  userid;
	int  typ;                       // sliceType
	int replicationId;
	bool last;
	bool computed;

	/* Execution Time */
	double start;
	double startProcessing;
	double end;
};

unsigned int print_ue(FILE *, const userS*,double ueet);

typedef struct {
	/* Place holders for computed results */
	int 				res_power[MAX_LAYERS][RX_ANT];
	scData_t 			layer_data[MAX_LAYERS][RX_ANT];
	complexMatrix_t 	comb_w[MAX_SC];
	#ifdef VERIFY_COMB_WC_PAR
	complexMatrix_t 	comb_w1[MAX_SC];
	#endif
	weightSC_t			combWeight[MAX_LAYERS];
	ltephygpp_complex_t 			symbols[NMB_SLOT*(OFDM_IN_SLOT-1)*MAX_SC*MAX_LAYERS];
	// ltephygpp_complex_t			symbols2[NMB_SLOT][(OFDM_IN_SLOT-1)][MAX_LAYERS][MAX_SC];
	ltephygpp_complex_t 			deint_symbols[2*(OFDM_IN_SLOT-1)*MAX_SC*MAX_LAYERS];
	complexMatrix_t 	R;
	int 				pow[RX_ANT][RX_ANT];
	char 				softbits[2*2*(OFDM_IN_SLOT-1)*MAX_SC*MOD_64QAM*MAX_LAYERS]; 
	#ifdef VERIFY_DEMAP_PAR
	char 				softbits_pth[2*2*(OFDM_IN_SLOT-1)*MAX_SC*MOD_64QAM*MAX_LAYERS];
	#endif
	unsigned char 		bits[2*(OFDM_IN_SLOT-1)*MAX_SC*MOD_64QAM/24*MAX_LAYERS];
} symbol_data;

typedef struct task task;

struct task {
	/* Meta info */
  	int 			nmbLayer;
	int 			startSc;
	int 			nmbSc;
	mod_type 		mod;
	task_type 		type;
	symbol_data		*symbolData;	/* variables for intermediate computations */
	input_data		*data;			/* Actual Input Data */

	task *next;

	/* task Id information */
	int subframe;
	int userid;
	int taskid;
	int slotid;

	/* Synchornization variables */
	volatile bool computed;

#ifdef WS_TIMING_ANALYSES
	int seqid;
	int bs;
	int crnti;
	double arrival;
	double start;
	double stop;
#endif
};


typedef struct user_queue user_queue;

struct user_queue {
	pthread_mutex_t 		lock;
	userS 					*first;
	userS 					*last;

	/* This approach might lead to problems */
	volatile unsigned int 	qcount;
	volatile unsigned int 	subframes_count;
};

typedef struct task_queue task_queue;

struct task_queue {
	pthread_mutex_t lock;
	task 			*first;
	task 			*last;
	volatile unsigned int qcount;
};

typedef struct queues queues;
struct queues {
	user_queue users;
	task_queue threads[MAX_THREADS];
};

typedef struct {
	short 	tid;
	short  	*active;
	queues 	*queue;
	bool    ht_enable;
	unsigned int total_app_cpus; /* Total number of cpus for LTE system (Daemon + Master Threads)*/
	unsigned int aff;			 /* CPU affinity */
} task_args;

void uplink_user(task_queue *queue, userS *user, task *tasks,int tid);
void handle_task(task_queue* queue, unsigned int tid, int stid);
void compute_antcomb(task *task);
void compute_micf(task *task);
void compute_combwc(task *task);
void compute_demap(task *task);
void *uplink_task(void *args);
double get_maxdiff_comb_w(complexMatrix_t* w1, complexMatrix_t* w2);
void uplink_ws_nosimd(int num_cpus,const char *file_name,const char *dump_file_name,const char *phetFileName,int version);
uint64_t compute_wkld_vol(const userS *user);
typedef bool (*monitorDrops)(const userS *user); /* Selectively monitor drops of only certains UEs */

/* Risk Measure associated with subframe drops */
struct AtomicMVGCounter_t {
	pthread_mutex_t lock;
	double counter;
	int N;
};

/* ----------------------Global Variables------------------------------------*/
/* Declare the Global Variables */
symbol_data global_symbolData[MAX_THREADS];
task        global_tasks[MAX_THREADS][MAX_TASKS];
input_data  data[NMB_DATA_FRAMES];

/* These threads exploit inter user lavel parallelism */
pthread_t threads[MAX_THREADS];
task_args args[MAX_THREADS];

/* Reading the trace files */
FILE* trc;

#ifdef INSTRUMENTUE
/* Performance Data files */
FILE* ueetGroundTruthFD;
#define PDCBUFSIZE BUFSIZ*BUFSIZ
char ueetGroundTruthFDbuf[PDCBUFSIZE];
#endif

#ifdef KNOCK_OFF_COMPUTATION
/* Dump subframes in IMDEA format*/
FILE* convertToIMDEAFormatFD;
#endif

/* start time */
struct timespec start_time;

/* Drop Counters */
atomic_uint dropsEMBB, dropsAVT, dropsIoT;
atomic_uint totalEMBB, totalAVT, totalIoT;
double get_alpha(int typ);
double get_deadline(int typ);

#ifdef WS_TIMING_ANALYSES
char phaseEtGroundTruthFDbuf[PDCBUFSIZE];
FILE* phaseEtGroundTruthFD;
#endif
#endif /* _UPLINK_H */