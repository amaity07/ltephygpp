#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>
#include "def.h"
#include "kernel_def.h"
#include "uplink.h"
#include "uplink_parameters.h"

#define MAX12(a,b) (a > b ? a : b)
/* Display a ue */
unsigned int print_ue(FILE *fd,const userS *uep,double ueet) {
	char modstr[BUFSIZ];
	switch(uep->mod) {
		case MOD_PSK 	: sprintf(modstr,"%s","MOD_PSK");   break;
		case MOD_QPSK 	: sprintf(modstr,"%s","MOD_QPSK");  break;
		case MOD_16QAM  : sprintf(modstr,"%s","MOD_16QAM"); break;
		case MOD_64QAM  : sprintf(modstr,"%s","MOD_64QAM"); break;
		default			: fprintf(stderr,"Unable to print, modulation scheme not supported %d\n",uep->mod);
						  exit(EXIT_FAILURE);
	}

	fprintf(fd,"ue_%d_%d_%d.csv,slice:%d,sf:%d,ueid:%d,layer:%d,startRB:%d,prb:%d,mod:%s,last:%s,ueet:%.2f\n",\
	uep->seqid,uep->bs,uep->crnti,\
	uep->typ,uep->subframe,uep->userid,\
	uep->nmbLayer,uep->startRB,\
    uep->nmbRB,modstr,\
	(uep->last)?"True":"False",ueet);
	return (uep->nmbRB);
}

void print_sf(FILE *fd, const user_parameters *sf) {
	userS *ue;
	unsigned int total_prbs = 0;
	unsigned int max_prbs   = 0;
	unsigned int nmbUsers   = 0;
	fprintf(fd,"-------------------\n");
	if (!sf) {
		fprintf(fd,"%d\n",0);
	} else {
		for(ue = sf->first; ue != NULL; ue = ue->next) {
			total_prbs += print_ue(fd,ue,-1);
			max_prbs    = MAX12(max_prbs,ue->nmbRB);
			nmbUsers++;
		}
    	// if (total_prbs > 100) {
    	//     fprintf(stderr,"Malformed Subframe@%d, total_prbs exceeds 100\n",sf->first->subframe);
		// 	exit(EXIT_FAILURE);
    	// }
    	// if (nmbUsers > 10) {
    	//     fprintf(stderr,"Malformed Subframe@%d, total_ue exceeds 10\n",sf->first->subframe);
    	//     exit(EXIT_FAILURE);
    	// }
    	if (!(sf->last->last)) {
    	    fprintf(stderr,"Malformed Subframe@%d, Last UE not appropriately tagged\n",sf->first->subframe);        //exit(EXIT_FAILURE);
    	}
		fprintf(fd,"Total PRBs allocated = %d\n",total_prbs);
		fprintf(fd,"Total UEs allocated = %d\n",nmbUsers);
	}
	fprintf(fd,"-------------------\n\n");
}
