/*******************************************************************************
 *                      LTE UPLINK RECEIVER PHY BENCHMARK                      *
 *                                                                             *
 * This file is distributed under the license terms given by LICENSE.TXT       *
 *******************************************************************************
 * Author: Magnus Sjalander                                                    *
 ******************************************************************************/

#ifndef _UPLINK_PARAMETERS_H
#define _UPLINK_PARAMETERS_H

#include "uplink.h"

#define COUNTDOWN 1

typedef struct {
  userS *first;
  userS *last;
} user_parameters;

void print_sf(FILE *, const user_parameters*);
typedef struct {
  short active;
  short inc;
  short count;
  short countdown;
  int active_min;
  int active_max;
  int active_sum;
  int active_frames;
  int activity[5];
  unsigned long long users_remaining;  // Raghu added: to pass on from user queue
  bool doneParametersGeneration;
} parameter_model;

void init_trace_file(const char *filename);
void init_parameter_model(parameter_model *pmodel);
void init_data(void);
user_parameters *uplink_parameters(parameter_model *pmodel);
user_parameters *uplink_parameters_constant(parameter_model *pmodel, unsigned int prb);
void init_globals(unsigned int total_app_cpus,\
                  unsigned int cpu_list[],\
				          const char *traceFileName,\
				          const char *dumpFileName,\
                  const char *phetFileName,\
				          queues *queuep,\
				          parameter_model *pmodelp);

/*
 * Trace format version
 * 1. Nishant's INFOCOM version
 * 2. ICC 5G-MC version
 */
int uplink_parameters_ns3(parameter_model *pmodel,user_parameters **user_list, int version);

#endif /* _UPLINK_PARAMETERS_H */
