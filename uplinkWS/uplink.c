/*******************************************************************************
 *                      LTE UPLINK RECEIVER PHY BENCHMARK                      *
 *                                                                             *
 * This file is distributed under the license terms given by LICENSE.TXT       *
 *******************************************************************************
 * Author: Magnus Sjalander                                                    *
 ******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <time.h>
#include <math.h>
#include <fcntl.h>
#include <sys/syscall.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>
#include <ctype.h>
#include <ecotools/errordefs.h>
#include <ecotools/roi_hooks.h>
#include <ecotools/cpu_uarch.h>
#include "kernel_def.h"
#include "def.h"
#include "uplink.h"
#include "uplink_parameters.h"
#include "uplink_alarm.h"
#include "common_utils.h"
#include "crc_13.h"
#include "fft_8.h"
#include "par_limits.h"
#include "mf_4.h"
#include "soft_demap_9.h"
#include "ant_comb_7.h"
#include "chest_5.h"
#include "weight_calc_6.h"

static queues queue;

double getCurrentTime(struct timespec *offset) {
	struct timespec currentTime;
	clock_gettime(CLOCK_MONOTONIC,&currentTime);
	return calculate_time_diff_spec(currentTime,*offset)/1000000.0;
}

/* WS version of uplink */
void uplink_ws_nosimd(int num_cpus,\
                      const char *traceFileName,\
					  const char *dumpFileName,\
					  const char *phetFileName,\
					  int version) {

	#ifndef QUIET
	PRINTTOPO("%d Base-Stations, %d X %d MIMO",MAX_BS,MIMO_CONFIGURATION,MIMO_CONFIGURATION);
	#endif

	unsigned int cpu_alloc_list[MAX_THREADS];
	unsigned int i;
	for(i = 0; i < MAX_THREADS; i++) {
		if (i < num_cpus)
			cpu_alloc_list[i] = i;
		else 
			cpu_alloc_list[i] = 0;
	}

	/* Initialization */
	parameter_model pmodel;
	user_parameters *parameters; 
	unsigned int sfcount  = 0;
	unsigned int ue_in_subframe = 0;
	unsigned int subframes_dropped = 0;
	bool drop_subframes = false; /* This parameter is required when you have a finite queue */
	struct timespec t1;
	userS *ue;
	int prev1PRB = 0, prev2PRB = 0, prev3PRB = 0;
	int prev1UE = 0, prev2UE = 0, prev3UE = 0;
	char effectiveStartTime[26];
	char effectiveEndTime[26];
	init_globals(num_cpus,\
	             cpu_alloc_list,\
				 traceFileName,\
				 dumpFileName,\
				 phetFileName,\
				 &queue,\
				 &pmodel);
	#ifndef QUIET
	PRINTTOPO("Started Generation");
	#endif


	time_t timerStart = time(NULL);
	struct tm* effectiveStartTimeInfo  = localtime(&timerStart);
	strftime(effectiveStartTime,26,"%Y-%m-%d %H:%M:%S",effectiveStartTimeInfo);
	clock_gettime(CLOCK_MONOTONIC,&start_time);
	/* Initialize the alarm */
	uplink_alarm_init(DELTA);
	while (1) {
		if (uplink_parameters_ns3(&pmodel,&parameters,version) < 0)
			break;

		#ifdef PRINT_HOOKS
		print_sf(stdout,parameters);
		#endif

		#ifndef KNOCK_OFF_COMPUTATION
		/* Wait until next subframe should be computed */
		uplink_wait_for_alarm();
		if (parameters) {
			if(!drop_subframes) {
				/* attach timing info for instrumentation */
				for (ue = parameters->first; ue != NULL; ue=ue->next) {
					clock_gettime(CLOCK_MONOTONIC,&t1);
					ue->start = calculate_time_diff_spec(t1,start_time)/1000000.0;

					/* Ignore the is first CACHE_WARMUP_SUBFRAMES subframes */
					#ifdef CACHE_WARMUP_SUBFRAMES
					if (sfcount >= CACHE_WARMUP_SUBFRAMES) {
					#endif
						switch(ue->typ) {
							case TRAFFIC_TYPE_AVT: atomic_fetch_add(&totalAVT,1); break;
							case TRAFFIC_TYPE_EMBB: atomic_fetch_add(&totalEMBB,1); break;
							case TRAFFIC_TYPE_IOT: atomic_fetch_add(&totalIoT,1);break;
							default : atomic_fetch_add(&totalEMBB,1); break;
						}
					#ifdef CACHE_WARMUP_SUBFRAMES
					}
					#endif
				}
				pthread_mutex_lock(&queue.users.lock);
				if (queue.users.first == NULL)
					queue.users.first 		= parameters->first;
				else
					queue.users.last->next 	= parameters->first;
				queue.users.last 			= parameters->last;
				queue.users.qcount 		   += parameters->last->userid + 1;
				queue.users.subframes_count++;
				pthread_mutex_unlock(&queue.users.lock);
			}
			else
				subframes_dropped++;
			free(parameters);
		}
		#else  /* KNOCK_OFF_COMPUTATION */
		/* Convert the trace into IMDEA like trace for further processing*/
		if (parameters) {
			if(!drop_subframes) {
				for (ue = parameters->first; ue != NULL; ue=ue->next) {
					int sfn = sfcount / NMB_SUBFRAMES_PER_FRAME;
					int sfi = sfcount % NMB_SUBFRAMES_PER_FRAME;
					int crnti = 10000 + rand() % 53000; // Random CRNTI
					int mcs = -1;
					int layers = ue->nmbLayer;
					int prbs = ue->nmbRB;
					fprintf(convertToIMDEAFormatFD,"%d\t%d\t%d\t%d\t%d\t%d\n",\
						sfn,sfi,crnti,\
						layers,prbs,ue->mod);
				}
			}
		}
		#endif /* KNOCK_OFF_COMPUTATION */
		sfcount++;
	}
	
	#ifndef KNOCK_OFF_COMPUTATION
	#ifndef QUIET
	/* Wait for all the threads to finish */
	PRINTTOPO("Finished Generating Subframes");
	#endif
	while(queue.users.qcount > 0);
	time_t timerEnd = time(NULL);
	struct tm* effectiveEndTimeInfo    = localtime(&timerEnd);
	strftime(effectiveEndTime,26,"%Y-%m-%d %H:%M:%S",effectiveEndTimeInfo);
	#ifndef QUIET
	PRINTTOPO("%d Subframes executed",sfcount);
	PRINTTOPO("\nTotal(EMBB|AVT|IOT),Drops(EMBB|AVT|IOT),EffectStart,EffectEnd\nTBSC,%d,%d,%d,%d,%d,%d,%s,%s\n\n\n",\
	atomic_load(&totalEMBB),atomic_load(&totalAVT),atomic_load(&totalIoT),\
	atomic_load(&dropsEMBB),atomic_load(&dropsAVT),atomic_load(&dropsIoT),\
	effectiveStartTime,effectiveEndTime);
	#endif
	#ifdef INSTRUMENTUE
	#ifndef QUIET
	PRINTTOPO("All Subframes dumped");
	#endif
	fclose(ueetGroundTruthFD);
	#endif
	#else
	fclose(convertToIMDEAFormatFD);
	#endif /* KNOCK_OFF_COMPUTATION */
	#ifdef WS_TIMING_ANALYSES
	fclose(phaseEtGroundTruthFD);
	#endif
}