/*******************************************************************************
 *                      LTE UPLINK RECEIVER PHY BENCHMARK                      *
 *                                                                             *
 * This file is distributed under the license terms given by LICENSE.TXT       *
 *******************************************************************************
 * Author: Magnus Sjalander                                                    *
 ******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <stdatomic.h>
#include <ecotools/errordefs.h>
#include "par_limits.h"
#include "common_utils.h"
#include "uplink.h"
#include "uplink_parameters.h"
#include "def.h"
#include "kernel_def.h"

/* Create Data Frames */
void init_data(void) {
	int ofdm, frame, ue;
	int i,j,slot,bs;
	unsigned short int inc = 0;
	unsigned short int dec = 16500;

	for (frame=0; frame<NMB_DATA_FRAMES; frame++) {
		for (slot=0; slot<NMB_SLOT; slot++) {
			for (ofdm=0; ofdm<OFDM_IN_SLOT; ofdm++) {
				for (i=0; i<MAX_SC; i++) {
					data[frame].in_data[slot][ofdm][0][i].re = rand();
					data[frame].in_data[slot][ofdm][0][i].im = rand();
					data[frame].in_data[slot][ofdm][1][i].re = rand();
					data[frame].in_data[slot][ofdm][1][i].im = rand();
					data[frame].in_data[slot][ofdm][2][i].re = rand();
					data[frame].in_data[slot][ofdm][2][i].im = rand();
					data[frame].in_data[slot][ofdm][3][i].re = rand();
					data[frame].in_data[slot][ofdm][3][i].im = rand();
				}
			}
			for (i=0; i<MAX_SC; i++) {
				data[frame].fftw[slot][i].re = rand();
				data[frame].fftw[slot][i].im = rand();
			}

			/* reference symbol in layer */
			for (i=0; i<MAX_SC; i++) {
				for (j=0; j<MAX_LAYERS; j++) {
					data[frame].in_rs[slot][i][j].re = dec--;
					data[frame].in_rs[slot][i][j].im = inc++;
				}
			}
		}
	}
}

void init_trace_file(const char *filename) {
	trc = fopen(filename,"r"); /* check the file format */
	char log[BUFSIZ];
	if(!trc) {
		PRINTERROR("Error opening trace file : %s",filename);
	}
	#ifndef QUIET
	PRINTTOPO("Trace File %s initialized",filename);
	#endif
}

void init_parameter_model(parameter_model *pmodel) {
	pmodel->active    = MAX_THREADS;
	pmodel->countdown = COUNTDOWN;
	pmodel->count     = 1;
	pmodel->inc       = 1;
	pmodel->active_min = 100;
	pmodel->active_max = 0;
	pmodel->active_sum = 0;
	pmodel->active_frames = 0;
	pmodel->activity[0] = 0;
	pmodel->activity[1] = 0;
	pmodel->activity[2] = 0;
	pmodel->users_remaining = 0;
	pmodel->doneParametersGeneration = false;
}

/* uses Nishant's (NS3) traces format */
int uplink_parameters_ns3(parameter_model *pmodel, user_parameters **user_list, int version) {
	int active = 0;
	float load_perc = 0.0;
	int userid = 0, prbs, layers, mod, num_users = 0, max_users = 0;
	static int subframeId = 0;
	int subframe = 0, totalRBs = 0, totalUE = 0;
	struct userS *user = NULL;
	int seqid=-1,bs=-1,crnti=-1,typ=-1;
	int replicationId=-1;
	
	if(trc) {
		fscanf(trc,"%d\n",&num_users);
		userid = 0;
		max_users = num_users;
		
		if(max_users == 0)
			*user_list = NULL;
		
		while(num_users--) {
			switch (version) {
				case 1 : fscanf(trc,"%d %d %d\n",&prbs,&layers,&mod); break;
				case 2 : fscanf(trc,"%d %d %d %d %d %d %d %d\n",&seqid,&bs,&crnti,&prbs,&layers,&mod,&typ,&replicationId); break;
				default : fscanf(trc,"%d %d %d\n",&prbs,&layers,&mod); break;
			}
		
			/* Allocate a new user and add it to the parameter list*/
			if(user == NULL) {
 				user = (struct userS*)malloc(sizeof(struct userS));
				user->next = NULL;
				*user_list = (user_parameters*)malloc(sizeof(user_parameters));
				(*user_list)->first = user;
			}
			else {
				user->next = (struct userS*)malloc(sizeof(struct userS));
				user = user->next;
				user->next = NULL;
			}
			(*user_list)->last = user;
			
			if(user) {
				/* Assign the values read to the users  */
				user->seqid    = seqid;
				user->bs       = bs;
				user->crnti    = crnti;
				user->typ      = typ;
				user->subframe = subframeId;
				user->userid   = userid;
				user->startRB  = 0;     // Assuming different users will have different data
				user->nmbRB	   = prbs;
				switch (mod) {
					case 1 : user->mod = MOD_QPSK;
							 break;
					case 2 : user->mod = MOD_16QAM;
							 break;
					case 3 : user->mod = MOD_64QAM;
							 break;
					case 4 : user->mod = MOD_64QAM;
							 break;
					default : user->mod = MOD_QPSK;
				}
				user->nmbLayer = layers;
				user->data     = &data[subframe%NMB_DATA_FRAMES];
				user->next 	   = NULL;
				user->last	   = (user->userid == max_users - 1)?true:false;
				user->replicationId = replicationId;

				user->start    = -1;
				user->end      = -1;
				userid++;
				totalRBs += prbs;
				totalUE++;
			}
			else {
				fprintf(stderr,"user creation failed 2\n");
				exit(EXIT_FAILURE);
			}
		}
		if (fscanf(trc,"---------- %d\n",&subframe) == EOF) {
			fclose(trc);
			return -1;
		}
		/* Error Checks */
		if (version == 2) {
			if (subframeId != subframe) {
				PRINTERROR("subframeId:%d not properly aligned to trace:%d",subframeId,subframe);
			} 
		} else {
			if (subframeId != subframe-1) {
				PRINTERROR("subframeId:%d not properly aligned to trace:%d",subframeId,subframe);
			}
		}

		if (totalRBs > MAX_RB*MAX_BS) {
			PRINTERROR("subframeId:%d contains %d PRBs higher than %d supported",subframeId,totalRBs,MAX_RB);
		}
		if (totalUE > MAX_USERS*MAX_BS) {
			PRINTERROR("subframeId:%d contains %d UEs higher than %d supported",subframeId,totalUE,MAX_USERS);
		}
		subframeId++;
	}
	else {
		PRINTERROR("trc is NULL, reading failed\n");
		*user_list = NULL;
	}
	return 0;
}

int uplink_parameters_warmup(int numue, int numsf, int prbs, user_parameters **user_list) {
	int userid = 0;
	static int totalsf = 1;
	struct userS *user = NULL;
	int subframe = 0;
	int maxue = numue;
	if (totalsf > numsf) {
		*user_list = NULL;
		return -1;
	}
	while(numue--) {
		if(user == NULL) {
 			user = (struct userS*)malloc(sizeof(struct userS));
			user->next = NULL;
			*user_list = (user_parameters*)malloc(sizeof(user_parameters));
			(*user_list)->first = user;
		} else {
			user->next = (struct userS*)malloc(sizeof(struct userS));
			user = user->next;
			user->next = NULL;
		}
		(*user_list)->last = user;
		if(user) {
			user->bs       = 46111;
			user->crnti    = 8123;
			user->typ      = 1;
			user->subframe = totalsf;
			user->userid   = userid++;
			user->startRB  = 0;     // Assuming different users will have different data
			user->nmbRB	   = prbs;
			user->mod      = MOD_64QAM;
			user->nmbLayer = 4;
			user->data     = &data[subframe%NMB_DATA_FRAMES];
			user->next 	   = NULL;
			user->last	   = (user->userid == maxue - 1)?true:false;
			user->start    = -1;
			user->end      = -1;
		}
	}
	totalsf++;
	return 0;
}
