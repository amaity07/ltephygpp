/*******************************************************************************
 *                      LTE UPLINK RECEIVER PHY BENCHMARK                      *
 *                                                                             *
 * This file is distributed under the license terms given by LICENSE.TXT       *
 *******************************************************************************
 * Author: Magnus Sjalander                                                    *
 ******************************************************************************/

#ifndef _DEF_H
#define _DEF_H

#include <pthread.h>
#include <math.h>
#include "kernel_def.h"
#include "par_limits.h"

/* Number of data frames for which input data should be generated */
#define NMB_DATA_FRAMES   	300

/* Subframes per frame */
#define NMB_SUBFRAMES_PER_FRAME 10

/* Time between subframes in us */
#define DELTA             1000

#define COMB_WC_PAR
#define DEMAP_PAR

/* Verify the parallelized phases */
#ifdef COMB_WC_PAR
//#define VERIFY_COMB_WC_PAR
#endif

#ifdef DEMAP_PAR
//#define VERIFY_DEMAP_PAR
#endif

//#define DROP_ENABLED
#ifdef DROP_ENABLED
#define DEADLINE 2500
#endif

/* HOOKS */
//#define PRINT_HOOKS
//#define UTIL_HOOKS            /* Enable the collection of Utilization Data of all the cpus */

#define INSTRUMENTUE
// #define QUIET

#define AFFINE_MIGRATION_SPIN 1000

#define TRAFFIC_TYPE_IOT  0
#define TRAFFIC_TYPE_EMBB 2
#define TRAFFIC_TYPE_AVT  1
#define DEADLINE_EMBB     2.5
#define DEADLINE_AVT      0.5
#define DEADLINE_IOT      2.5

/* CACHE WARMUP routines */
#define CACHE_WARMUP_SUBFRAMES 1000 /* Ignore the first hundred subframes */

/* Do not processing subframes rather dump in IMDEA format */
// #define KNOCK_OFF_COMPUTATION


/* Work Stealing Timing Analyses */
// #define WS_TIMING_ANALYSES

/* Timing utilities */
double getCurrentTime(struct timespec *offset);
#endif /* _DEF_H */
