/*******************************************************************************
 *                      LTE UPLINK RECEIVER PHY BENCHMARK                      *
 *                                                                             *
 * This file is distributed under the license terms given by LICENSE.TXT       *
 *******************************************************************************
 * Author: Magnus Sjalander                                                    *
 ******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sched.h>
#include <time.h>
#include <ecotools/cpu_uarch.h>
#include <ecotools/errordefs.h>
#include "common_utils.h"
#include "uplink.h"
#include "par_limits.h"


void handle_user(user_queue *queue, task_queue *tqueue,\
                 task *tasks,int tid) {
	userS  *user 	= NULL;
	
	/* deq_user_q */
	pthread_mutex_lock(&(queue->lock));
	if (queue->first) {
	  user 			= queue->first;
	  queue->first 	= user->next;
	}
	pthread_mutex_unlock(&(queue->lock));

	/* process user */	
	if (user) {
		uplink_user(tqueue, user, tasks,tid);
		#ifdef INSTRUMENTUE
		fprintf(ueetGroundTruthFD,"ue_%d_%d_%d.csv,%d,%d,%d,%f,%f,%f\n",\
			user->seqid,user->bs,user->crnti,\
			user->subframe,user->typ,user->replicationId,\
			user->start,user->startProcessing,user->end);
		#endif
		
		/* Decrement the subframe count */
		pthread_mutex_lock(&(queue->lock));
	  	queue->qcount--;
	  	if(queue->qcount == 0 && queue->subframes_count > 0) {
			queue->subframes_count--;
		}
		pthread_mutex_unlock(&(queue->lock));
		free(user);
	}
}

/*
 * tid  : Thread id of the thread that creates the task
 * stid : Thread id of the thread that executes the task 
 */
void handle_task(task_queue *queue, unsigned int tid, int stid) {

	task *taskp = NULL;

	struct timespec task_comp_start_time, task_comp_end_time;
	//long long cpu_time_used;

	/* deq_task_q */
	pthread_mutex_lock(&(queue->lock));
	if (queue->qcount) {
		taskp = queue->first;
		queue->first = taskp->next;
		queue->qcount--;
	}
	pthread_mutex_unlock(&(queue->lock));
	
	char* type = "UNSUPPORTED";
	if (taskp) {
		#ifdef WS_TIMING_ANALYSES
		taskp->start = getCurrentTime(&start_time);
		#endif
		switch(taskp->type) {
			case CHANNEL:
			{
				compute_micf(taskp);
				type = "MICF";
				break;
			}
			case SYMBOL:
			{
				compute_antcomb(taskp);
				type = "ANTCOMB";
				break;
			}
			#ifdef COMB_WC_PAR
			case COMBWCALC:
			{
				compute_combwc(taskp);
				type = "COMBWC";
				break;
			}
			#endif /* COMB_WC_PAR */
			#ifdef DEMAP_PAR
			case DEMAP:
			{
				compute_demap(taskp);
				type = "DEMAP";
				break;
			}
			#endif /* DEMAP_PAR */
			default:
			{
				fprintf(stderr,"subframe@%d,user@%d,task@%d:Encountered unsupported task type: %i\n",\
			  	 taskp->subframe, taskp->userid,taskp->taskid,taskp->type);
				exit(EXIT_FAILURE);
			}
		}
		#ifdef WS_TIMING_ANALYSES
		taskp->stop = getCurrentTime(&start_time);
		fprintf(phaseEtGroundTruthFD,"ue_%d_%d_%d.csv,%d,"
				"%s,%d,%d,"
				"%d,%d,"
				"%f,%f,"
				"%f\n",
				taskp->seqid,taskp->bs,taskp->crnti,taskp->subframe,
				type,taskp->taskid,taskp->slotid,
				tid,stid,
				taskp->arrival,taskp->start,
				taskp->stop);
		#endif
	}
}

int find_work(task_queue *queues, const int tid) {
	int i;
	for (i=0; i<MAX_THREADS; i++) {
		while (queues[i].qcount && i!=tid)
			handle_task(&queues[i],tid,i);
	}
	return 0;
}

void *uplink_task(void *args) {

	task_args *targs         = (task_args *)args;
	struct user_queue *user_queue   = &targs->queue->users;
	task_queue *task_queue   = &targs->queue->threads[targs->tid];
	symbol_data *symbolData  = &global_symbolData[targs->tid];
	task *tasks              = &global_tasks[targs->tid][0];
	int tid				   	 = targs->tid;
	unsigned int i;
	bool ht_enable                 = targs->ht_enable;
	unsigned int total_app_cpus    = targs->total_app_cpus;
	unsigned int cpu_affity        = (tid+1)%total_app_cpus;
	
	//affinity_set_cpu(tid);
	
	assert((tid < MAX_THREADS) && (tid > -1));
	
	/* Set up necessary structures */
	unsigned int lim = MAX_TASKS;
	for (i = 0; i < lim; i++) {
		//printf("slave%d : i = %d, lim = %d, exit? = %s\n",tid,i,MAX_TASKS,(i < MAX_TASKS)?"true":"false");
		tasks[i].symbolData = symbolData;
	}
	//printf("Done\n");
	//exit(EXIT_FAILURE);

	
	/* Look for work to perform */
	while(1) {
		if (user_queue->first /*&& tid == 0*/)
			handle_user(user_queue, task_queue,tasks, tid);
		else
		  	find_work(targs->queue->threads, tid);
		// sleep(10000);
	}
}
