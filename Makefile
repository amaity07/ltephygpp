ROOT          = ${PWD}
BUILD_DIR     = ${ROOT}/build
KERNELS_DIR   = ${ROOT}/lte-kernels/src
WS_DIR        = ${ROOT}/uplinkWS
BMAC_DIR      = ${ROOT}/uplinkBMACFit
SRCS_DIR      = ${KERNELS_DIR} ${WS_DIR}
SRCS_DIR2     = ${KERNELS_DIR} ${BMAC_DIR}
LIBS	      = -ldl -lpthread -lm -lecotools
EXECUTABLE    = ws_nosimd
EXECUTABLE2   = bmacfit_nosimd #${executableBMAC}

SRCS := $(shell find ${SRCS_DIR} -name "*.c") ws_main.c
OBJS := $(SRCS:%=$(BUILD_DIR)/%.o)
DEPS := $(OBJS:.o=.d)

SRCS2 := $(shell find ${SRCS_DIR2} -name "*.c") bmacfit_main.c
OBJS2 := $(SRCS2:%=$(BUILD_DIR)/%.o)
DEPS2 := $(OBJS2:.o=.d)

# Compiler Specific optimization options
CC		       := ${cc}
INCLUDES       := -I${KERNELS_DIR} -I${WS_DIR} -I${BMAC_DIR}
DFLAGS	       := -DTEST_KERN -DMAX_BS=${bs}

# Compiler Specific optimization options
ifeq (${cc},icc)
	#CFLAGS     = -xHost -xMIC-AVX512 -MMD -MD
	CFLAGS     = -xHost -MMD -MD 
	# CFLAGS     = -O0 -pg -xMIC-AVX512 -MMD -MD -gdwarf-3 -fstack-security-check
	#CFLAGS     = -O3 -xMIC-AVX512 -MMD -MD
else
	CFLAGS	   = -O3
	#CFLAGS	   = -O0 -g
endif

.PHONY: all
all: ws bmacfit

ws: $(OBJS)
	$(CC) $(CFLAGS) $(DFLAGS) $(OBJS) $(LIB_DIR) -o $(EXECUTABLE) $(LIBS)

bmacfit: $(OBJS2)
	$(CC) $(CFLAGS) $(DFLAGS) $(OBJS2) -o $(EXECUTABLE2) $(LIBS)

$(BUILD_DIR)/%.c.o: %.c
	$(MKDIR_P) $(dir $@)
	$(CC) $(CFLAGS) $(DFLAGS) $(INCLUDES) -c $< -o $@

$(BUILD_DIR)/%.cpp.o: %.cpp
	$(MKDIR_P) $(dir $@)
	$(CC) $(CFLAGS) $(DFLAGS) $(INCLUDES) -c $< -o $@


.PHONY: clean
clean:
	rm -rf $(BUILD_DIR)

-include $(DEPS) $(DEPS2)
MKDIR_P ?= mkdir -p
