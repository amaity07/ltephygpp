This PHY benchmark is a realistic implementation of
the baseband processing for an LTE base station. The benchmark
implements a small portion of Uplink PHY layer of the LTE
standard.

# Obtaining the source
1. git clone -b <branch_name> --recursive  https://amaity07@bitbucket.org/amaity07/ltephygpp.git
The vectorized implementations (BPMaC-pB-SIMD,BPMaC-Full) are maintained in separate branches because they use entirely different DSP implementations.

# Compiler Requirements
1. Its is strongly advised to compile this benchmark using Intel Compiler.

# Building the executable
| Implementation | Branch | Compilation Options | Running the Executable |
| --- | --- | --- | --- |
| Baseline | master | 'make cc=icc bs=10' | Check test3.sh script |
| BPMaC-pB | master | 'make cc=icc bs=10 -DCOMB_WC_PAR -DDEMAP_PAR'. Altertanatively one can also uncomment the relevant options in uplink/def.h | Check test3.sh script |
| BPMaC-pB-SIMD | ws_simd | 'make cc=icc bs=10' | Check test2.sh script |
| BPMaC-Full | ws_simd | 'make cc=icc bs=10' | Check test2.sh script |

# Realistic Traces
|Trace|Filename|
|---|---|
|NS3HL|Traces/HLTrace1.v2.txt|
|NS3LL|Traces/LLTrace1.v2.txt|
|CVU1|Traces/CVU1.v2.txt|
|CVU2|Traces/CVU2.v2.txt|