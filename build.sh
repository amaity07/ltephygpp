#!/bin/bash

source /opt/intel/compilers_and_libraries_2019/linux/bin/compilervars.sh intel64

# Building
set -e
rm -rf ws_nosimd_* bmacfit_nosimd_* bpmac_nosimd_*
echo "Building..."
make cc=icc bs=10
bin1="ws_nosimd_$(hostname)_build_$(git log -1 '--pretty=format:%h')"
bin2="bpmac_nosimd_$(hostname)_build_$(git log -1 '--pretty=format:%h')"
mv ws_nosimd ${bin1}
mv bmacfit_nosimd ${bin2}

scp ${bin1} ${bin2} amaity@taurus:/home/amaity/Desktop/hostphypkg_ebe2e65/BinsExp/