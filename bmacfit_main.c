/* Isolated Timing Analysis */

#include <stdlib.h>
#include <stdio.h>
#include <ecotools/errordefs.h>
#include <ecotools/roi_hooks.h>
#include <ecotools/cpu_uarch.h>
#include <getopt.h>
#include <ctype.h>
#include "mf_4.h"
#include "fft_8.h"
#include "soft_demap_9.h"
#include "ant_comb_7.h"
#include "chest_5.h"
#include "weight_calc_6.h"
#include "uplink.h"
#include "def.h"
#include "common_utils.h"
#include "phy_ul_multifit.h"

void help() {
	PRINTTOPO("Usage: ./<prog> -m <num of cpus> -f <Trace file name> -v <version> -d <UE timing dump>"); 
	exit(EXIT_SUCCESS);
}

int main(int argc, char *argv[]) {
	if(argc < 2) {
		fprintf(stderr,"Enter the of RBs\n");
		exit(EXIT_FAILURE);
	}
	int nmbRB = 100;

	#ifndef KNOCK_OFF_COMPUTATION_WSSIMD
	// PRINTTOPO("---------------Prefetcher Warmup Begin---------------");	
	// /* Phase 1 computations */
	// mf_test(nmbRB);
	// fft_test(nmbRB); // Used twice in phase 1, also used in phase 3
	// window_test(nmbRB);
	
	// /* Phase 2 computations */
	// mmse_by_cholsolve_RXxX_complex_partiled_test(nmbRB);
	
	// /* Phase 3 computations */
	// ant_comb_test(nmbRB);
	
	// /* Phase 4 Computation */
	// soft_demap_pthread_test(nmbRB);
	// PRINTTOPO("---------------Prefetcher Warmup END-----------------");	
	
	/* A typical lte phy ul application */
	/**
 	 * Please not the commenting the previous 
 	 * kernel calls results in a very bloated
 	 * and unpredictable execution time,
 	 * Please investigate and document the
 	 * reasons. 
 	 * --------------------------------------
 	 */
	int c;
	char *fval = NULL;
	char *mval = NULL;
	char *vval = NULL;
	char *dval = NULL;
	while ((c = getopt (argc, argv, "hm:f:v:d:")) != -1) {
		switch (c) {
			case 'm' : mval = optarg; break;
			case 'f' : fval = optarg; break;
			case 'v' : vval = optarg; break;
			case 'd' : dval = optarg; break;
			case 'h' : help(); break;
			case '?' : {
				if (optopt == 'm' || optopt == 'f' || optopt == 'v' || optopt == 'd')
					PRINTERROR("Option -%c requires an argument\n", optopt);
			}
		}
	}
	if (!fval) {
		help();
		PRINTERROR("Trace file not found");
	}
	if (!dval) {
		help();
		PRINTERROR("UEET file not found");
	}
	// if (!eval) {
	// 	help();
	// 	PRINTERROR("TaskEt file not found");
	// }
	int m = atoi(mval);
	int v = atoi(vval);
    phy_G_multifit(m,fval,dval,v);
	#else
	printf("Computation Knocked Off");
	#endif
}
