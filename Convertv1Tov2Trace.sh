#!/bin/bash

echo "Convert v1 to IMDEA (like trace) trace"
source /opt/intel/compilers_and_libraries_2019/linux/bin/compilervars.sh intel64

allBS=(10)
echo ${allBS}
traceFile=Traces/LLTrace1
for bssize in ${allBS[@]}; do
    echo "BS ${bssize}"
    rm -rf ws_nosimd*
    make clean
    make cc=icc bs=${bssize}
   	./ws_nosimd -m 24 -f ${traceFile}.txt -w 100 -v 1 -d ${traceFile}.imdea.txt -e /dev/null -g /dev/null
done
chown -R amaity:amaity ${PWD}

#r -m 24 -f Traces/HLTrace1.txt -w 100 -v 1 -d Traces/HLTrace1.imdea.txt -e /dev/null -g /dev/null